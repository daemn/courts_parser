from config import app_config
from datetime import datetime
import logging as log
from typing import Any
import abc

__doc__ = """Менеджер базы данных.

Судебное дело идентифицируется по комбинации полей office и number. Предполагаю, что эти поля всегда присутствуют и
такая комбинация уникальна для каждого дела.

Служебные поля create_time и update_time устанавливаются автоматически. Содержат время создания и время обновления
записи в базе.

Прочие поля не обязательны и их список может меняться.

Атрибуты дел:
create_time Время добавления судебного дела.
update_time Время обновления судебного дела.
number      Номер дела на сайте (обязательный).
office      Судебный участок (обязательный).
url         Ссылка на дело.
category    Категория дела.
judge       Судья.
judgment    Судебное решение.
date        Дата судебного решения.
plaintiff   Истец.
defendant   Ответчик.
article     Статья кодекса.
code        Идентификатор кодекса.

>>> from database_mongodb import CaseDatabase
>>> CaseDatabase.connect(
...     user='mongo_user',
...     password='mongo_password',
...     host='mongo_host',
...     port=27017,
... )
>>> db = CaseDatabase(database='mongo_database', collection='cases')
>>> db.put(case={...})
>>> db.count(filter_={...})
1
>>> CaseDatabase.disconnect()
"""

log.basicConfig(level=log.getLevelName(app_config['log_level']), format='%(levelname)s : %(filename)s : %(message)s')


class DatabaseError(Exception):
    """Общая ошибка при работе с базой данных."""
    pass


class DatabaseConnectError(DatabaseError):
    """Ошибка подключения к базе данных."""
    pass


class DatabaseRequestError(DatabaseError):
    """Ошибка исполнения запроса к базе данных."""
    pass


class ABCCase(abc.ABC, dict):
    """Судебное дело."""

    @abc.abstractmethod
    def new_and_changed_attrs(self, other: dict) -> dict:
        """Возвращает только изменившиеся атрибуты."""
        pass

    @abc.abstractmethod
    def base_types(self) -> dict:
        """Возвращает словарь с данными судебного дела, приведёнными с базовым типам."""
        pass


class ABCCaseFilter(abc.ABC, dict):
    """Фильтр судебных дел."""

    @abc.abstractmethod
    def check(self) -> None:
        """Проверяет корректность фильтра."""
        pass

    @abc.abstractmethod
    def mongo_filter(self) -> dict:
        """Собирает mongo-фильтр для запроса к базе."""
        pass


class ABCCasesDatabase(abc.ABC):
    """Базовый класс интерфейса базы данных для судебных дел."""

    @abc.abstractmethod
    def put(self, case: dict) -> None:
        """Создаёт или обновляет судебное дело в базе."""
        pass

    @abc.abstractmethod
    def count(self, filter_: dict) -> int:
        """Подсчитывает количество судебных дел."""
        pass

    @abc.abstractmethod
    def get(self, filter_: dict, skip: int, limit: int) -> list:
        """Возвращает судебные дела."""
        pass

    @abc.abstractmethod
    def delete_all(self) -> None:
        """Удаляет все судебные дела."""
        pass


class ABCParametersDatabase(abc.ABC):
    """Базовый класс интерфейса базы данных для параметров."""

    @abc.abstractmethod
    def put(self, name: str, value: Any) -> None:
        """Создаёт или обновляет параметр в базе."""
        pass

    @abc.abstractmethod
    def get(self, name: str) -> Any:
        """Возвращает значение параметра."""
        pass

    @abc.abstractmethod
    def delete_all(self) -> None:
        """Удаляет все параметры."""
        pass


class Case(ABCCase):
    """Судебное дело."""

    def __init__(self, data: dict = None) -> None:
        super().__init__()
        if data is not None:
            for key, value in data.items():
                self.__setitem__(key, value)

    def __setitem__(self, key, value) -> None:
        """Атрибуты с датами приводит к типу datetime."""
        if key in ('date', 'create_time', 'update_time') and not isinstance(value, datetime):
            value = datetime(value[0], value[1], value[2])
        super().__setitem__(key, value)

    def __eq__(self, other: dict) -> bool:
        """Сравнивает два судебных дела, игнорируя служебные атрибуты."""
        mask = {'_id', 'create_time', 'update_time'}
        self_attrs = set(self.keys()) - mask
        other_attrs = set(other.keys()) - mask
        if self_attrs != other_attrs:
            return False
        return all([self[a] == other[a] for a in self_attrs])

    def new_and_changed_attrs(self, other: dict) -> dict:
        """Возвращает только новые и изменившиеся атрибуты.

        :return: Словарь с обновлёнными атрибутами.
        """
        result: dict = {}
        for key in other.keys():
            if key not in self or self[key] != other[key]:
                result[key] = other[key]
        return result

    def base_types(self) -> dict:
        """Возвращает данные судебного дела, приведённые к базовым типам.

        :return: Словарь с атрибутами дела.
        """
        result: dict = {}
        for key, value in self.items():
            if key in ('date', 'create_time', 'update_time'):
                value = [value.year, value.month, value.day]
            elif key == '_id':
                value = str(value)
            result[key] = value
        return result
