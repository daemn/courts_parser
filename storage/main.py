#!/bin/env python3
from config import app_config
from database import DatabaseError, Case
from database_mongodb import CaseFilter, CasesDatabase, ParametersDatabase
import logging as log
from typing import Optional, Callable
import atexit
from flask import Flask, request, escape, jsonify

__doc__ = """WEB-API модуля storage.
Все данные передаются в формате json в виде словаря.

POST /case          - Сохранить в базу судебное дело.

        Обязательные поля:
    office: str     - Название судебного участка.
    number: str     - Номер судебного дела в реестре судебного участка.
    code: str       - Кодекс.

        Пример запроса:
    POST /case
    {
        "case": { "number": "xxx", "office": "Судебный участок ...", "code": "civ", "date": [ 2022, 3, 8 ] }
    }

GET  /case          - Прочитать из базы судебные дела.

        Кроме атрибутов судебных дел, принимаются поля:
    skip: int       - Пропустить первые N результатов.
    limit: int      - Вернуть N судебных дел.

        Пример запроса:
    GET /case
    {
        "filter": { "code": "civ", "date_from": [ 2022, 3, 8 ], "defendant": "Иванов Иван" },
        "skip": 0,
        "limit": 30
    }

GET /case/quantity  - Подсчитать количество дел подходящих под запрос.

        Пример запроса:
    GET /case/quantity
    {
        "filter": { "code": "civ", "date_from": [ 2022, 3, 8 ], "defendant": "Иванов Иван" }
    }

POST /parameter     - Сохранить в базе параметр.

        Обязательные поля:
    name: str       - Имя параметра.
    value: Any      - Значение параметра.

        Пример запроса:
    POST /parameter
    {
        "name": "sadness value",
        "value": 100500
    }

GET /parameter      - Прочитать из базы значение параметра.

        Обязательные поля:
    name: str       - Имя параметра.

        Пример запроса:
    GET /parameter
    {
        "name": "sadness value"
    }

"""

log.basicConfig(level=log.getLevelName(app_config['log_level']), format='%(levelname)s : %(filename)s : %(message)s')
app = Flask(__name__)


def catch_exceptions(func: Callable):
    """Выполняет функцию, ловя все исключения."""
    def exec_this() -> tuple:
        try:
            return func()

        except (TypeError, ValueError, KeyError, IndexError, AttributeError) as err:
            log.error(f'Wrong data. {err}.')
            return '', 415  # Some attribute is mistyped or JSON error.

        except DatabaseError as err:
            log.error(f'DatabaseError. {err}.')
            return '', 500  # Database trouble.

        except Exception as err:
            log.error(f'Unexpected. {err}.')
            return '', 500  # Unexpected trouble.

    exec_this.__name__ = func.__name__
    exec_this.__doc__ = func.__doc__
    return exec_this


@catch_exceptions
def process_post_case() -> tuple:
    """Сохраняет дело в базе."""
    request_data: Optional[dict] = request.get_json()

    case: Case = Case(request_data['case'])
    if not case:
        raise ValueError('Empty case.')
    for required_attr in 'office', 'number', 'code':
        if required_attr not in case:
            raise KeyError(f'Required attribute "{required_attr}" is missed.')

    db = CasesDatabase()
    db.put(case)

    return '', 200


@catch_exceptions
def process_get_case() -> tuple:
    """Возвращает список судебных дел по запросу."""
    request_data: Optional[dict] = request.get_json()

    skip: int = request_data.get('skip', 0)
    limit: int = request_data.get('limit', 50)
    filter_: CaseFilter = CaseFilter(request_data['filter'])
    filter_.check()

    db = CasesDatabase()
    found_cases = []
    for case in db.get(filter_=filter_, skip=skip, limit=limit):
        found_cases.append(case.base_types())

    return jsonify({'found_cases': found_cases}), 200


@catch_exceptions
def process_get_case_quantity() -> tuple:
    """Возвращает количество судебных дел соответствующих запросу."""
    request_data: Optional[dict] = request.get_json()

    filter_: CaseFilter = CaseFilter(request_data['filter'])
    filter_.check()

    db = CasesDatabase()
    value: int = db.count(filter_=filter_)

    return jsonify({'value': value}), 200


@catch_exceptions
def process_post_parameter() -> tuple:
    """Сохраняет параметр в базе."""
    request_data: Optional[dict] = request.get_json()

    for attribute in 'name', 'value':
        if attribute not in request_data:
            raise ValueError(f'Required attribute "{attribute}" is missed.')

    db = ParametersDatabase()
    db.put(name=request_data['name'], value=request_data['value'])
    return '', 200


@catch_exceptions
def process_get_parameter() -> tuple:
    """Возвращает значение параметра с указанным именем."""
    request_data: Optional[dict] = request.get_json()
    if 'name' not in request_data:
        raise ValueError('Required attribute "name" is missed.')

    db = ParametersDatabase()
    value = db.get(name=request_data['name'])
    return jsonify({'value': value}), 200


@app.route('/__doc__', methods=['GET'])
def get_doc() -> str:
    return f'<pre>{escape(__doc__)}</pre>'


@app.route('/case', methods=['POST'])
def post_case() -> tuple:
    """Сохраняет дело в базе."""
    return process_post_case()


@app.route('/case', methods=['GET'])
def get_case() -> tuple:
    """Возвращает список судебных дел по запросу."""
    return process_get_case()


@app.route('/case/quantity', methods=['GET'])
def get_case_quantity() -> tuple:
    """Возвращает количество судебных дел соответствующих запросу."""
    return process_get_case_quantity()


@app.route('/parameter', methods=['POST'])
def post_parameter() -> tuple:
    """Сохраняет параметр в базе."""
    return process_post_parameter()


@app.route('/parameter', methods=['GET'])
def get_parameter() -> tuple:
    """Возвращает значение параметра с указанным именем."""
    return process_get_parameter()


def exit_handler() -> None:
    """Выполняет процедуры корректного завершения работы программы."""
    pass


def main() -> None:
    atexit.register(exit_handler)
    app.run(
        debug=(app_config['log_level'] == 'DEBUG'),
        host=app_config['bind_host'],
        port=app_config['bind_port']
    )


if __name__ == '__main__':
    main()
