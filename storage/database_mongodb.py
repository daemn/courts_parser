from config import app_config
from database import Case, ABCCaseFilter, ABCCasesDatabase, ABCParametersDatabase
from database import DatabaseError, DatabaseRequestError, DatabaseConnectError
import logging as log
from pymongo import MongoClient
from pymongo import collection as mongo_collection
from pymongo.errors import PyMongoError, OperationFailure
from datetime import datetime
from typing import Any, Iterable, Optional

__doc__ = """Интерфейс базы данных mongodb."""


log.basicConfig(level=log.getLevelName(app_config['log_level']), format='%(levelname)s : %(filename)s : %(message)s')


class CaseFilter(ABCCaseFilter):
    """Фильтр судебных дел."""

    def __init__(self, data: dict = None) -> None:
        super().__init__()
        if data is not None:
            for key, value in data.items():
                self.__setitem__(key, value)

    def __setitem__(self, key, value) -> None:
        if key in ('date_from', 'date_to') and not isinstance(value, datetime):
            value = datetime(value[0], value[1], value[2])
        super().__setitem__(key, value)

    def check(self) -> None:
        """Проверяет корректность фильтра.

        :raise ValueError: Пустой фильтр.
        """
        if not self:
            raise ValueError('Empty filter.')

    def mongo_filter(self) -> dict:
        """Собирает mongo-фильтр для запроса к базе.

        :return: Фильтр в формате mongo.
        """

        # Если в фильтре указан _id, то нет смысле проверять остальные поля.
        if '_id' in self:
            return {'_id': self['_id']}

        mongo_filter = {}
        date_filter = {}
        for key, value in self.items():
            if key == 'date_from':
                date_filter['$gte'] = value
            elif key == 'date_to':
                date_filter['$lte'] = value
            else:
                # {'office': 'chel kirova 10'}
                # {'office': {'$regex': 'chel.*kirova.*10', '$options': 'i'}}
                search_words = list(filter(lambda a: a, value.split()))
                mongo_filter[key] = {'$regex': '.*'.join(search_words), '$options': 'i'}
        if date_filter:
            # {'date_from': datetime.datetime(2021, 1, 25, 0, 0), 'date_to': datetime.datetime(2021, 1, 29, 0, 0)}
            # {'date': {'$gte': ISODate("2021-12-14T00:00:00.000Z"), '$lte': ISODate("2021-12-15T00:00:00.000Z")}}
            mongo_filter['date'] = date_filter

        return mongo_filter


class MongoDBConnector:
    """Соединение с базой данных MongoDB."""

    user: str = app_config['mongo_user']
    password: str = app_config['mongo_password']
    host: str = app_config['mongo_host']
    port: int = app_config['mongo_port']
    database: str = app_config['mongo_database']
    _db_client: Optional[MongoClient] = None

    @classmethod
    def connect(cls) -> None:
        """Соединяется с базой данных.

        :raise DatabaseConnectError: Проблема с подключением.
        """
        if cls._db_client is None:
            connect_uri: str = f'mongodb://{cls.user}:{cls.password}@{cls.host}:{cls.port}'
            log.debug(f'Try to connect to "{connect_uri}".')
            try:
                cls._db_client = MongoClient(connect_uri)
            except (PyMongoError, OperationFailure) as err:
                raise DatabaseConnectError from err

    @classmethod
    def disconnect(cls) -> None:
        """Отсоединяется от базы данных.

        :raise DatabaseConnectError: Проблема с отключением.
        """
        log.debug('Try to disconnect from database.')
        try:
            if cls._db_client is not None:
                cls._db_client.close()
                cls._db_client = None
        except (PyMongoError, OperationFailure) as err:
            raise DatabaseRequestError from err

    def __init__(self) -> None:
        self.connect()

    def __del__(self) -> None:
        self.disconnect()

    def collection(self, name: str) -> mongo_collection:
        """Возвращает указатель на коллекцию.

        :param name: Имя коллекции.
        """
        return self._db_client[self.database][name]


class CasesDatabase(ABCCasesDatabase):
    """Интерфейс базы данных MongoDB для судебных дел."""

    def __init__(self) -> None:
        self.collection_name = app_config['mongo_cases_collection']
        self._col: mongo_collection = MongoDBConnector().collection(self.collection_name)

    def put(self, case: Case) -> None:
        """Добавляет дело в базу.

        :param case: Судебное дело.
        :raise DatabaseRequestError: Ошибка запроса к базе.
        """
        try:
            result: dict = self._col.find_one({'number': case['number'], 'office': case['office']})
            if result is None:
                case['create_time'] = datetime.utcnow()
                self._col.insert_one(case)
            else:
                stored_case = Case(result)
                if case != stored_case:
                    case['update_time'] = datetime.utcnow()
                    update_fields: dict = stored_case.new_and_changed_attrs(case)
                    self._col.update_one(
                        filter={'_id': stored_case['_id']},
                        update={'$set': update_fields},
                        upsert=True,
                    )
        except PyMongoError as err:
            raise DatabaseRequestError from err

    def count(self, filter_: CaseFilter) -> int:
        """Подсчитывает количество судебных дел по фильтру.

        :param filter_: Фильтр дел.
        :return: Количество дел в базе.
        :raise DatabaseRequestError: Проблема с запросом к базе.
        """
        try:
            return self._col.count_documents(filter_.mongo_filter())
        except PyMongoError as err:
            raise DatabaseRequestError from err

    def get(self, filter_: CaseFilter, skip: int = 0, limit: int = 1) -> Iterable:
        """Возвращает судебные дела по одному.

        :param filter_: Фильтр дел.
        :param skip: Пропустить первые N результатов.
        :param limit: Максимальное количество дел в ответе.
        :return: Список дел.
        :raise DatabaseRequestError: Ошибка запроса к базе.
        """
        try:
            for object_ in self._col.find(filter_.mongo_filter(), limit=limit, skip=skip):
                yield Case(object_)
        except PyMongoError as err:
            raise DatabaseRequestError from err

    def delete_all(self) -> None:
        """Удаляет все судебные дела из базы.

        :raise DatabaseRequestError: Ошибка запроса к базе.
        """
        try:
            self._col.delete_many({})
        except PyMongoError as err:
            raise DatabaseRequestError from err


class ParametersDatabase(ABCParametersDatabase):
    """Базовый класс интерфейса базы данных для параметров."""

    def __init__(self) -> None:
        self.collection_name = app_config['mongo_parameters_collection']
        self._col: mongo_collection = MongoDBConnector().collection(self.collection_name)

    def put(self, name: str, value: Any) -> None:
        """Создаёт или обновляет параметр в базе.

        :param name: Имя параметра.
        :param value: Значение параметра.
        :raise DatabaseRequestError: Ошибка запроса к базе.
        """
        log.debug(f'Try to put parameter "{name}" = "{value}" to base.')
        try:
            self._col.update_one({'name': name}, {'$set': {'value': value}}, upsert=True)
        except PyMongoError as err:
            raise DatabaseRequestError from err

    def get(self, name: str) -> Any:
        """Возвращает значение параметра.

        :param name: Имя параметра.
        :return: Значение параметра.
        :raise DatabaseRequestError: Ошибка запроса к базе.
        """
        try:
            parameter = self._col.find_one({'name': name})
            return parameter['value']
        except PyMongoError as err:
            raise DatabaseRequestError from err
        except TypeError:
            return None

    def delete_all(self) -> None:
        """Удаляет все параметры.

        :raise DatabaseRequestError: Ошибка запроса к базе.
        """
        try:
            self._col.delete_many({})
        except PyMongoError as err:
            raise DatabaseRequestError from err
