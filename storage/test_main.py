from datetime import datetime
from typing import Any, Iterable
import pytest
import flask.testing
from main import app
import main
from database import Case, ABCCaseFilter, ABCCasesDatabase, ABCParametersDatabase, DatabaseError


def test_case_data_with_simple_date() -> dict:
    return {
        'number':    '123',
        'category':  'Категория',
        'office':    'Участок',
        'judge':     'Судья',
        'judgment':  'Решение',
        'date':      (2021, 7, 15),
        'plaintiff': 'Истец',
        'defendant': 'Ответчик',
        'url':       'http://office/case',
        'code':      'civ',
    }


class MockCasesDatabase(ABCCasesDatabase):

    requests: list = []
    answers: list = []

    def put(self, case: Case) -> None:
        self.requests.append(case)

    def count(self, filter_: ABCCaseFilter) -> int:
        self.requests.append(filter_)
        answer = self.answers.pop(0)
        if answer == DatabaseError:
            raise answer
        return answer

    def get(self, filter_: ABCCaseFilter, skip: int = 0, limit: int = 1) -> Iterable:
        self.requests.append((filter_, skip, limit))
        answer = self.answers.pop(0)
        if answer == DatabaseError:
            raise answer
        for thing in answer:
            yield thing

    def delete_all(self) -> None:
        self.requests.append('delete_all')


class MockParametersDatabase(ABCParametersDatabase):

    requests: list = []
    answers: list = []

    def put(self, name: str, value: Any) -> None:
        self.requests.append((name, value))

    def get(self, name: str) -> Any:
        self.requests.append(name)
        answer = self.answers.pop(0)
        if answer == DatabaseError:
            raise answer
        return answer

    def delete_all(self) -> None:
        self.requests.append('delete_all')


main.CasesDatabase = MockCasesDatabase
main.ParametersDatabase = MockParametersDatabase


class TestGetCasesQuantity:
    """Запрос количества дел в базе."""

    @pytest.fixture
    def client(self) -> flask.testing.FlaskClient:
        with app.test_client() as client:
            yield client

    def test_wrong_type(self, client) -> None:
        """Неправильные типы данных в запросе."""
        answer = client.get('/case/quantity', json=['woo'])
        assert answer.status_code == 415

    def test_wrong_attrs(self, client) -> None:
        """Неправильные аргументы в запросе."""
        answer = client.get('/case/quantity', json={'filter': {'foo': 'bar'}})
        assert answer.status_code == 415

    def test_valid_database_request(self, client) -> None:
        """Корректный запрос."""
        MockCasesDatabase.requests = []
        MockCasesDatabase.answers = [3]
        answer = client.get('/case/quantity', json={'filter': {'defendant': 'woo woo'}})
        assert answer.status_code == 200
        assert answer.json == {'value': 3}
        assert MockCasesDatabase.requests == [{'defendant': 'woo woo'}]

    def test_valid_database_request_with_dates(self, client) -> None:
        """Корректный запрос с датами."""
        MockCasesDatabase.requests = []
        MockCasesDatabase.answers = [3]
        answer = client.get(
            '/case/quantity',
            json={'filter': {'office': 'woo', 'date_from': [2010, 1, 1], 'date_to': [2020, 2, 2]}}
        )
        assert answer.status_code == 200
        assert answer.json == {'value': 3}
        assert MockCasesDatabase.requests == [
            {'office': 'woo', 'date_from': datetime(2010, 1, 1), 'date_to': datetime(2020, 2, 2)}
        ]

    def test_database_error(self, client) -> None:
        """База возвращает эксепшен."""
        MockCasesDatabase.requests = []
        MockCasesDatabase.answers = [DatabaseError]
        answer = client.get('/case/quantity', json={'filter': {'defendant': 'woo'}})
        assert answer.status_code == 500


class TestGetCase:
    """Запрос судебного дела из базы."""

    @pytest.fixture
    def client(self) -> flask.testing.FlaskClient:
        with app.test_client() as client:
            yield client

    def test_wrong_type(self, client) -> None:
        """Неправильные типы данных в запросе."""
        answer = client.get('/case', json=['bar'])
        assert answer.status_code == 415

    def test_prepare_database_request(self, client) -> None:
        """Корректные запросы."""
        MockCasesDatabase.requests = []
        MockCasesDatabase.answers = [[]]
        answer = client.get('/case', json={'filter': {'foo': 'woo woo'}})
        assert answer.status_code == 200
        assert answer.json == {'found_cases': []}
        assert MockCasesDatabase.requests == [({'foo': 'woo woo'}, 0, 50)]

    def test_database_error(self, client) -> None:
        """База возвращает эксепшен."""
        MockCasesDatabase.requests = []
        MockCasesDatabase.answers = [DatabaseError]
        answer = client.get('/case', json={'filter': {'foo': 'bar'}})
        assert answer.status_code == 500

    def test_no_found_cases(self, client) -> None:
        """База возвращает пустой список."""
        MockCasesDatabase.requests = []
        MockCasesDatabase.answers = [[]]
        answer = client.get('/case', json={'filter': {'defendant': 'woo'}})
        assert answer.status_code == 200
        assert answer.json == {'found_cases': []}
        assert MockCasesDatabase.requests == [({'defendant': 'woo'}, 0, 50)]

    def test_found_case(self, client) -> None:
        """База возвращает дело."""
        MockCasesDatabase.requests = []
        MockCasesDatabase.answers = [[Case({'_id': 1, 'number': 'a1', 'date': datetime(2022, 1, 12)})]]
        answer = client.get('/case', json={'filter': {'number': 'a1'}})
        assert answer.status_code == 200
        assert answer.json == {'found_cases': [{'_id': '1', 'number': 'a1', 'date': [2022, 1, 12]}]}
        assert MockCasesDatabase.requests == [({'number': 'a1'}, 0, 50)]

    def test_found_many_cases(self, client) -> None:
        """База возвращает много дел."""
        MockCasesDatabase.requests = []
        MockCasesDatabase.answers = [[
            Case({'_id': '1', 'number': 'a1', 'date': datetime(2022, 1, 12)}),
            Case({'_id': '2', 'number': 'a2', 'date': datetime(2022, 1, 13)}),
            Case({'_id': '3', 'number': 'a3', 'date': datetime(2022, 1, 14)}),
        ]]
        answer = client.get('/case', json={'filter': {'defendant': 'woo'}})
        assert answer.status_code == 200
        assert answer.json == {
            'found_cases': [
                {'_id': '1', 'number': 'a1', 'date': [2022, 1, 12]},
                {'_id': '2', 'number': 'a2', 'date': [2022, 1, 13]},
                {'_id': '3', 'number': 'a3', 'date': [2022, 1, 14]},
            ],

        }
        assert MockCasesDatabase.requests == [({'defendant': 'woo'}, 0, 50)]


class TestPostCase:

    @pytest.fixture
    def client(self) -> flask.testing.FlaskClient:
        with app.test_client() as client:
            yield client

    def test_valid(self, client) -> None:
        """Добавление нового дела в базу."""
        MockCasesDatabase.requests = []
        case_data: dict = test_case_data_with_simple_date()
        answer = client.post('/case', json={'case': case_data})
        assert answer.status_code == 200
        case_data['date'] = datetime(*case_data['date'])
        assert MockCasesDatabase.requests == [case_data]

    def test_empty(self, client) -> None:
        """Добавление дела без данных."""
        answer = client.post('/case')
        assert answer.status_code == 415

    def test_no_attrs(self, client) -> None:
        """Добавление дела без обязательных атрибутов."""
        case_no_number: dict = test_case_data_with_simple_date()
        del(case_no_number['number'])
        answer = client.post('/case', json={'case': case_no_number})
        assert answer.status_code == 415

        case_no_office: dict = test_case_data_with_simple_date()
        del(case_no_office['office'])
        answer = client.post('/case', json={'case': case_no_office})
        assert answer.status_code == 415

        case_no_civ: dict = test_case_data_with_simple_date()
        del(case_no_civ['office'])
        answer = client.post('/case', json={'case': case_no_civ})
        assert answer.status_code == 415

    def test_wrong_data_type(self, client) -> None:
        """Добавление дела с данными с некорректными типами."""
        answer = client.post('/case', json=[{"foo": "bar"}])
        assert answer.status_code == 415

        answer = client.post('/case', json='')
        assert answer.status_code == 415

        answer = client.post('/case', json=42)
        assert answer.status_code == 415

        case_wrong_date: dict = test_case_data_with_simple_date()
        case_wrong_date['date'] = '42'
        answer = client.post('/case', json={'case': case_wrong_date})
        assert answer.status_code == 415

        case_wrong_date['date'] = ['wtf']
        answer = client.post('/case', json={'case': case_wrong_date})
        assert answer.status_code == 415


class TestPostParameter:
    """Добавление параметра."""

    @pytest.fixture
    def client(self) -> flask.testing.FlaskClient:
        with app.test_client() as client:
            yield client

    def test_empty(self, client) -> None:
        """Нет данных."""
        answer = client.post('/parameter')
        assert answer.status_code == 415

    def test_no_attrs(self, client) -> None:
        """Нет обязательных атрибутов."""
        answer = client.post('/parameter', json={'name': 'foo'})
        assert answer.status_code == 415

        answer = client.post('/parameter', json={'value': 'bar'})
        assert answer.status_code == 415

    def test_wrong_data_type(self, client) -> None:
        """Данные с некорректными типами."""
        answer = client.post('/parameter', json=[{"foo": "bar"}])
        assert answer.status_code == 415

        answer = client.post('/parameter', json='')
        assert answer.status_code == 415

        answer = client.post('/parameter', json=42)
        assert answer.status_code == 415

    def test_valid(self, client) -> None:
        """Правильные данные."""
        MockParametersDatabase.requests = []
        answer = client.post('/parameter', json={'name': 'foo', 'value': 'bar'})
        assert answer.status_code == 200
        assert MockParametersDatabase.requests == [('foo', 'bar')]


class TestGetParameter:
    """Запрос параметра."""

    @pytest.fixture
    def client(self) -> flask.testing.FlaskClient:
        with app.test_client() as client:
            yield client

    def test_empty(self, client) -> None:
        """Нет данных."""
        answer = client.get('/parameter')
        assert answer.status_code == 415

    def test_no_attrs(self, client) -> None:
        """Нет обязательных атрибутов."""
        answer = client.get('/parameter', json={})
        assert answer.status_code == 415

    def test_wrong_data_type(self, client) -> None:
        """Данные с некорректными типами."""
        answer = client.get('/parameter', json=[{"foo": "bar"}])
        assert answer.status_code == 415

        answer = client.get('/parameter', json='')
        assert answer.status_code == 415

        answer = client.get('/parameter', json=42)
        assert answer.status_code == 415

    def test_present(self, client) -> None:
        """Правильные данные. База вернула значение параметра."""
        MockParametersDatabase.requests = []
        MockParametersDatabase.answers = [666]
        answer = client.get('/parameter', json={'name': 'foo'})
        assert answer.status_code == 200
        assert answer.json == {'value': 666}
        assert MockParametersDatabase.requests == ['foo']

    def test_not_present(self, client) -> None:
        """Правильные данные. База не вернула значение параметра."""
        MockParametersDatabase.requests = []
        MockParametersDatabase.answers = [None]
        answer = client.get('/parameter', json={'name': 'foo'})
        assert answer.status_code == 200
        assert answer.json == {'value': None}
        assert MockParametersDatabase.requests == ['foo']
