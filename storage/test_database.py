from datetime import datetime
from database import Case


class TestCase:
    """Судебное дело."""

    def test_init(self) -> None:
        """Инициализация нового фильтра."""
        case: Case = Case({'foo': 'bar', 'date': (1970, 1, 1)})
        assert case == {
            'foo': 'bar',
            'date': datetime(1970, 1, 1),
        }

        case: Case = Case()
        case['code'] = 'without world war'
        case['create_time'] = (1945, 5, 9)
        case['update_time'] = (2022, 2, 24)
        assert case == {
            'code': 'without world war',
            'create_time': datetime(1945, 5, 9),
            'update_time': datetime(2022, 2, 24),
        }

    def test_equality(self) -> None:
        """Одинаковые и не одинаковые судебные дела."""
        case1: Case = Case({'number': 't1', 'office': 'test_office'})
        case2: Case = Case({'number': 't1', 'office': 'test_office'})
        case1['defendant'] = 'somebody'
        case2['defendant'] = 'somebody'
        case1['create_time'] = datetime(2022, 2, 24)
        case2['update_time'] = datetime(2022, 2, 24)

        assert case1 == case2

        case2['number'] = 2
        assert case1 != case2

    def test_changed_attrs(self) -> None:
        """Обновление судебного дела."""
        case1: Case = Case({'number': 't1', 'office': 'test_office'})
        case2: Case = Case({'number': 't1', 'office': 'test_office'})
        case1['code'] = 'civ'
        case2['office'] = 'York'
        case2['plaintiff'] = 'Joker'

        difference: dict = case1.new_and_changed_attrs(case2)
        assert difference == {'office': 'York', 'plaintiff': 'Joker'}

    def test_base_types(self) -> None:
        """Поля разных типов."""
        case: Case = Case({
            'number': 'abc123',
            'office': 'nowhere',
            'date': datetime(2021, 1, 2),
            'create_time': datetime(2021, 2, 2),
            'update_time': datetime(2021, 3, 2),
            '_id': 123,
        })
        assert case.base_types() == {
            'number': 'abc123',
            'office': 'nowhere',
            'date': [2021, 1, 2],
            'create_time': [2021, 2, 2],
            'update_time': [2021, 3, 2],
            '_id': '123',
        }
