from datetime import datetime
import pytest
from database_mongodb import MongoDBConnector
from database_mongodb import CaseFilter, CasesDatabase
from database_mongodb import ParametersDatabase
from database import Case
from typing import List


def new_test_case(number: int = 1) -> Case:
    return Case({
        'number': str(number),
        'office': 'test_office',
    })


class TestCaseFilter:
    """Фильтр судебных дел."""

    def test_init(self) -> None:
        """Инициализация нового фильтра."""
        filter_: CaseFilter = CaseFilter({'foo': 'bar', 'date_from': (1970, 1, 1)})
        assert filter_ == {
            'foo': 'bar',
            'date_from': datetime(1970, 1, 1),
        }

        filter_: CaseFilter = CaseFilter()
        filter_['code'] = 'without world war'
        filter_['date_from'] = (1945, 5, 9)
        filter_['date_to'] = (2022, 2, 24)
        assert filter_ == {
            'code': 'without world war',
            'date_from': datetime(1945, 5, 9),
            'date_to': datetime(2022, 2, 24),
        }

    def test_check(self) -> None:
        """Корректные и не корректные фильтры."""
        filter_: CaseFilter = CaseFilter()
        with pytest.raises(ValueError):
            filter_.check()

        filter_: CaseFilter = CaseFilter({'number': 'foo bar', 'code': 'woo'})
        filter_.check()

    def test_generate_filter(self) -> None:
        """Генерация монго-фильтра."""
        filter_: CaseFilter = CaseFilter()
        mongo_filter: dict = filter_.mongo_filter()
        assert mongo_filter == {}

        filter_['number'] = '1'
        mongo_filter: dict = filter_.mongo_filter()
        assert mongo_filter == {'number': {'$regex': '1', '$options': 'i'}}

        filter_['defendant'] = 'foo bar  woo'
        mongo_filter: dict = filter_.mongo_filter()
        assert mongo_filter == {
            'number': {'$regex': '1', '$options': 'i'},
            'defendant': {'$regex': 'foo.*bar.*woo', '$options': 'i'},
        }


class TestConnect:
    """Соединение с базой данных."""

    def test_success(self) -> None:
        """Достаточно прав для запроса."""
        db = MongoDBConnector()
        collection = db.collection('some_collection')
        collection.count_documents(filter={})


class TestCasesDatabase:
    """Интерфейс базы судебных дел."""

    @pytest.fixture
    def db(self) -> CasesDatabase:
        """Подключение к базе данных."""
        db = CasesDatabase()
        db.delete_all()
        yield db
        db.delete_all()

    def test_put_get_count(self, db: CasesDatabase) -> None:
        """Разные дела добавляются, читаются, считаются."""
        case1: Case = new_test_case(1)
        case2: Case = new_test_case(2)
        db.put(case1)
        db.put(case2)

        case_quantity: int = db.count(filter_=CaseFilter())
        assert case_quantity == 2

        stored_cases: List = list(db.get(filter_=CaseFilter(), limit=10))
        assert len(stored_cases) == 2

    def test_update(self, db: CasesDatabase) -> None:
        """Обновление дела."""
        case1: Case = new_test_case(1)
        case2: Case = new_test_case(1)
        case2['code'] = 'civ'
        db.put(case1)
        db.put(case2)

        case_quantity: int = db.count(filter_=CaseFilter())
        assert case_quantity == 1

        stored_cases: list = list(db.get(filter_=CaseFilter()))
        assert stored_cases[0] == case2
        assert stored_cases[0]['code'] == 'civ'

    def test_limit_skip(self, db: CasesDatabase) -> None:
        """Получение списка дел частями."""
        for number in range(50):
            db.put(new_test_case(number))
        assert db.count(filter_=CaseFilter()) == 50

        stored_cases: list = list(db.get(filter_=CaseFilter(), limit=10))
        assert len(stored_cases) == 10

        stored_cases: list = list(db.get(filter_=CaseFilter(), limit=12, skip=5))
        assert len(stored_cases) == 12


class TestParametersDatabase:
    """Интерфейс базы параметров."""

    @pytest.fixture
    def db(self) -> ParametersDatabase:
        """Подключение к базе данных."""
        db = ParametersDatabase()
        db.delete_all()
        yield db
        db.delete_all()

    def test_put_get(self, db: ParametersDatabase) -> None:
        """Разные параметры добавляются, читаются."""
        db.put('param1', 42)
        db.put('param2', 24)

        value = db.get('param1')
        assert value == 42

        value = db.get('param2')
        assert value == 24

    def test_update(self, db: ParametersDatabase) -> None:
        """Обновление существующего параметра."""
        db.put('param1', 42)
        db.put('param1', 24)

        value = db.get('param1')
        assert value == 24

    def test_get_not_present(self, db: ParametersDatabase) -> None:
        """Такого параметра нет."""
        value = db.get('param0')
        assert value is None
