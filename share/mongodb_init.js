// Создать индексы.

// in courts database.
db.parameters.createIndex({'name': 1}, {'unique': true})
db.cases.createIndex({'number': 1, 'office': 1}, {'unique': true})
db.cases.createIndex({'office': 1})
db.cases.createIndex({'category': 1})
db.cases.createIndex({'judge': 1})
db.cases.createIndex({'judgment': 1})
db.cases.createIndex({'article': 1})
db.cases.createIndex({'plaintiff': 1})
