import requests
from requests.exceptions import RequestException
import json
import logging as log
from datetime import datetime
from copy import copy
from config import app_config
from typing import Optional
import abc

__doc__ = """Интерфейс для работы с хранилищем."""

log.basicConfig(level=log.getLevelName(app_config['log_level']), format='%(levelname)s : %(filename)s : %(message)s')


class CaseStorageError(Exception):
    """Ошибка при обращении к хранилищу судебных дел."""
    pass


class ABCCaseStorage(abc.ABC):
    """Базовый класс сохранятеля судебных дел в базу."""

    @abc.abstractmethod
    def save_case(self, case: dict) -> None:
        """Сохраняет судебное дело в базу хранилища."""
        pass

    @abc.abstractmethod
    def set_max_case_date(self, office: str, code: str, max_date: datetime) -> None:
        """Сохраняет максимальную дату судебных дел."""
        pass

    @abc.abstractmethod
    def get_max_case_date(self, office: str, code: str) -> Optional[datetime]:
        """Возвращает максимальную дату судебных дел."""
        pass


class CaseWebStorage(ABCCaseStorage):
    """Сохранятель судебных дел в базу через Web API."""

    server_url: str = ''  # Адрес сервера хранилища.

    def save_case(self, case: dict) -> None:
        """Сохраняет судебное дело в базу хранилища.

        :param case: Судебное дело.
        :raise CaseStorageError: Проблема при обращении к хранилищу.
        """
        try:
            log.info(f'Try to save case "{case["number"]}" to storage.')
            case = copy(case)
            if 'date' in case:
                case['date'] = [case['date'].year, case['date'].month, case['date'].day]
            response = requests.post(url=self.server_url + '/case', json={'case': case})
            if response.status_code != 200:
                raise CaseStorageError(f'Storage server return code {response.status_code}.')
        except RequestException as err:
            raise CaseStorageError('Save case failed.') from err
        except KeyError as err:
            raise CaseStorageError(f'No required case attribute. {err}') from err

    def set_max_case_date(self, office: str, code: str, max_date: datetime) -> None:
        """Сохраняет максимальную дату судебных дел.

        :param office: Судебный участок.
        :param code: Кодекс.
        :param max_date: Дата.
        :raise CaseStorageError: Проблема при обращении к хранилищу.
        """
        log.info(f'Try to set max date for office:code "{office}:{code}" to storage.')
        parameter_name = f'{office}:{code}_max_date'
        parameter_value = [max_date.year, max_date.month, max_date.day]
        try:
            response = requests.post(
                url=self.server_url + '/parameter', json={'name': parameter_name, 'value': parameter_value}
            )
            if response.status_code != 200:
                raise CaseStorageError(f'Storage server return code {response.status_code}.')
        except RequestException as err:
            raise CaseStorageError('Storage request failed.') from err

    def get_max_case_date(self, office: str, code: str) -> Optional[datetime]:
        """Возвращает максимальную дату судебных дел.

        :param office: Судебный участок.
        :param code: Кодекс.
        :return: Максимальная дата среди сохранённых дел.
        :raise CaseStorageError: Проблема при обращении к хранилищу.
        """
        log.info(f'Try to get max date for office:code "{office}:{code}" from storage.')
        parameter_name = f'{office}:{code}_max_date'
        try:
            response = requests.get(
                url=self.server_url + '/parameter', json={'name': parameter_name}
            )
            if response.status_code != 200:
                raise CaseStorageError(f'Storage server return code {response.status_code}.')
        except RequestException as err:
            raise CaseStorageError('Storage request failed.') from err
        if not response.text:
            return None
        value = json.loads(response.text)['value']
        if value is None:
            return None
        return datetime(*value)
