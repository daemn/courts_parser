from typing import Iterator, Optional
import pytest
from datetime import datetime
import main
from main import Timer
from main import BadPagesStorage
from main import ABCDownloader, DownloadError
from main import OfficeCheckerCiv, OfficeCheckerAdm, CheckerError
from parser import ABCOfficesParser, ABCCasesParser, ParserError
from case_storage import ABCCaseStorage, CaseStorageError
import os
from tempfile import TemporaryDirectory


def new_test_civ_case(postfix: str) -> dict:
    return {
        'number':    f'test/190{postfix}',
        'category':  f'Категория_{postfix}',
        'office':    f'Участок_{postfix}',
        'judge':     f'Судья_{postfix}',
        'judgment':  f'Решение_{postfix}',
        'date':      datetime(2021, 7, 15),
        'plaintiff': f'Истец_{postfix}',
        'defendant': f'Ответчик_{postfix}',
        'url':       f'http://case{postfix}',
        'code':      'civ',
    }


def new_test_adm_case(postfix: str) -> dict:
    return {
        'number':    f'test/290{postfix}',
        'office':    f'Участок_{postfix}',
        'judge':     f'Судья_{postfix}',
        'judgment':  f'Решение_{postfix}',
        'date':      datetime(2021, 7, 15),
        'defendant': f'Ответчик_{postfix}',
        'article':   f'Статья_{postfix}',
        'url':       f'http://case{postfix}',
        'code':      'adm',
    }


class MockDownloader(ABCDownloader):

    requests: list = []
    results: list = [None]

    def get(self, url: str) -> bytes:
        self.requests.append(url)
        result = self.results.pop(0)
        if result is None:
            self.status = 404
            raise DownloadError
        self.status = 200
        return result


class MockOfficesParser(ABCOfficesParser):

    requests: list = []
    results: list = [None]

    def __init__(self, page: bytes) -> None:
        super().__init__(page)
        self.requests.append(page)

    def offices(self) -> Iterator:
        result = self.results.pop(0)
        if result is None:
            raise ParserError
        for office in result:
            yield office


class MockCasesParser(ABCCasesParser):

    requests: list = []
    results: list = [None]

    def __init__(self, page: bytes) -> None:
        super().__init__(page)
        self.requests.append(page)

    def cases(self) -> Iterator:
        result = self.results.pop(0)
        if result is None:
            raise ParserError
        assert isinstance(result, list) or isinstance(result, tuple)
        for case in result:
            yield case

    def url_to_next_page(self) -> str:
        result = self.results.pop(0)
        if result is None:
            raise ParserError
        assert isinstance(result, str)
        return result


class MockCaseWebStorage(ABCCaseStorage):

    requests: list = []
    results: list = [None]

    def save_case(self, case: dict) -> None:
        self.requests.append(case)

    def set_max_case_date(self, office: str, code: str, max_date: datetime) -> None:
        self.requests.append({'office': office, 'code': code, 'max_date': max_date})

    def get_max_case_date(self, office: str, code: str) -> Optional[datetime]:
        result = self.results.pop(0)
        if result is None:
            raise CaseStorageError
        return result


main.WebDownloader = MockDownloader
main.OfficesParser = MockOfficesParser
main.CivCasesParser = MockCasesParser
main.AdmCasesParser = MockCasesParser
main.CaseWebStorage = MockCaseWebStorage


class TestBadPagesStorage:
    """Хранение 'плохих' страниц."""

    @pytest.fixture
    def tmpdir(self) -> str:
        return TemporaryDirectory().name

    def test_save(self, tmpdir: str) -> None:
        """Сохранение страницы в хранилище."""
        BadPagesStorage.use_dir(tmpdir)
        BadPagesStorage.max_files = 3
        storage = BadPagesStorage()

        storage.save(b'one')
        number_of_files = len(os.listdir(tmpdir))
        assert number_of_files == 1

    def test_delete_old(self, tmpdir: str) -> None:
        """Удаление лишних страниц из хранилища."""
        BadPagesStorage.use_dir(tmpdir)
        BadPagesStorage.max_files = 3
        storage = BadPagesStorage()

        storage.save(b'one')
        storage.save(b'two')
        number_of_files = len(os.listdir(tmpdir))
        assert number_of_files == 2
        storage.save(b'three')
        storage.save(b'four')
        number_of_files = len(os.listdir(tmpdir))
        assert number_of_files == 3


class TestOfficeCheckerCiv:
    """Проверка гражданских судебных дел."""

    @pytest.fixture
    def prepare_stage(self) -> None:
        MockDownloader.requests = []
        MockDownloader.results = [None]
        MockOfficesParser.requests = []
        MockOfficesParser.results = [None]
        MockCasesParser.requests = []
        MockCasesParser.results = [None]
        MockCaseWebStorage.requests = []
        MockCaseWebStorage.results = [None]

    def test_download_fail(self, prepare_stage: None) -> None:
        """Downloader генерирует исключение."""
        MockCaseWebStorage.results = [datetime(2022, 2, 21)]

        with pytest.raises(CheckerError):
            office = OfficeCheckerCiv(name='office1', url='https://office1.ru')
            office.grab_cases()
        assert len(MockDownloader.requests) == 1
        assert MockDownloader.requests[0].startswith('https://office1.ru/modules.php?name=sud_delo&g1_case_')

    def test_parse_fail(self, prepare_stage: None) -> None:
        """Парсер генерирует исключение."""
        MockCaseWebStorage.results = [datetime(2022, 2, 21)]
        MockDownloader.results = [b'<html><body>test page</body></html>']

        with pytest.raises(CheckerError):
            office = OfficeCheckerCiv(name='office1', url='https://office1.ru')
            office.grab_cases()
        assert len(MockDownloader.requests) == 1
        assert MockDownloader.requests[0].startswith('https://office1.ru/modules.php?name=sud_delo&g1_case_')

    def test_parse_single_page(self, prepare_stage: None) -> None:
        """Корректно парсится единственная страница."""
        case1 = new_test_civ_case('1')
        case2 = new_test_civ_case('2')
        MockCaseWebStorage.results = [datetime(2022, 2, 21)]
        MockDownloader.results = [b'<html><body>test page</body></html>']
        MockCasesParser.results = [[case1, case2], '']

        office = OfficeCheckerCiv(name='office1', url='https://office1.ru')
        office.grab_cases()
        assert len(MockCaseWebStorage.requests) == 3
        assert MockCaseWebStorage.requests[:2] == [case1, case2]
        assert MockCaseWebStorage.requests[2]['office'] == 'office1'
        assert len(MockDownloader.requests) == 1
        assert MockDownloader.requests[0].startswith('https://office1.ru/modules.php?name=sud_delo&g1_case_')

    def test_parse_multi_page(self, prepare_stage: None) -> None:
        """Корректно парсится поиск из нескольких страниц."""
        case1 = new_test_civ_case('1')
        case2 = new_test_civ_case('2')
        case3 = new_test_civ_case('3')
        MockCaseWebStorage.results = [datetime(2022, 2, 21)]
        MockDownloader.results = [
            b'<html><body>test page</body></html>',
            b'<html><body>test page a</body></html>',
            b'<html><body>test page b</body></html>',
        ]
        MockCasesParser.results = [
            [case1], '/next_page_a',
            [case2], '/next_page_b',
            [case3], '',
        ]

        office = OfficeCheckerCiv(name='office1', url='https://office1.ru')
        office.grab_cases()
        assert len(MockCaseWebStorage.requests) == 4
        assert MockCaseWebStorage.requests[:3] == [case1, case2, case3]
        assert MockCaseWebStorage.requests[3]['office'] == 'office1'
        assert len(MockDownloader.requests) == 3
        assert MockDownloader.requests[0].startswith('https://office1.ru/modules.php?name=sud_delo&g1_case_')
        assert MockDownloader.requests[1].startswith('https://office1.ru/next_page_a')
        assert MockDownloader.requests[2].startswith('https://office1.ru/next_page_b')


class TestOfficeCheckerAdm:
    """Проверка административных судебных дел."""

    @pytest.fixture
    def prepare_stage(self) -> None:
        MockDownloader.requests = []
        MockDownloader.results = [None]
        MockOfficesParser.requests = []
        MockOfficesParser.results = [None]
        MockCasesParser.requests = []
        MockCasesParser.results = [None]
        MockCaseWebStorage.requests = []
        MockCaseWebStorage.results = [None]

    def test_download_fail(self, prepare_stage: None) -> None:
        """Downloader генерирует исключение."""
        MockCaseWebStorage.results = [datetime(2022, 2, 21)]
        with pytest.raises(CheckerError):
            office = OfficeCheckerAdm(name='office1', url='https://office1.ru')
            office.grab_cases()
        assert len(MockDownloader.requests) == 1
        assert MockDownloader.requests[0].startswith('https://office1.ru/modules.php?name=sud_delo&adm_case')

    def test_parse_fail(self, prepare_stage: None) -> None:
        """Парсер генерирует исключение."""
        MockCaseWebStorage.results = [datetime(2022, 2, 21)]
        MockDownloader.results = [b'<html><body>test page</body></html>']

        with pytest.raises(CheckerError):
            office = OfficeCheckerAdm(name='office1', url='https://office1.ru')
            office.grab_cases()
        assert len(MockDownloader.requests) == 1
        assert MockDownloader.requests[0].startswith('https://office1.ru/modules.php?name=sud_delo&adm_case')

    def test_parse_single_page(self, prepare_stage: None) -> None:
        """Корректно парсится единственная страница."""
        case1 = new_test_adm_case('1')
        case2 = new_test_adm_case('2')
        MockCaseWebStorage.results = [datetime(2022, 2, 21)]
        MockDownloader.results = [b'<html><body>test page</body></html>']
        MockCasesParser.results = [[case1, case2], '']

        office = OfficeCheckerAdm(name='office1', url='https://office1.ru')
        office.grab_cases()
        assert len(MockCaseWebStorage.requests) == 3
        assert MockCaseWebStorage.requests[:2] == [case1, case2]
        assert MockCaseWebStorage.requests[2]['office'] == 'office1'
        assert len(MockDownloader.requests) == 1
        assert MockDownloader.requests[0].startswith('https://office1.ru/modules.php?name=sud_delo&adm_case')

    def test_parse_multi_page(self, prepare_stage: None) -> None:
        """Корректно парсится поиск из нескольких страниц."""
        case1 = new_test_adm_case('1')
        case2 = new_test_adm_case('2')
        case3 = new_test_adm_case('3')
        MockCaseWebStorage.results = [datetime(2022, 2, 21)]
        MockDownloader.results = [
            b'<html><body>test page</body></html>',
            b'<html><body>test page a</body></html>',
            b'<html><body>test page b</body></html>',
        ]
        MockCasesParser.results = [
            [case1], '/next_page_a',
            [case2], '/next_page_b',
            [case3], '',
        ]

        office = OfficeCheckerAdm(name='office1', url='https://office1.ru')
        office.grab_cases()
        assert len(MockCaseWebStorage.requests) == 4
        assert MockCaseWebStorage.requests[:3] == [case1, case2, case3]
        assert MockCaseWebStorage.requests[3]['office'] == 'office1'
        assert len(MockDownloader.requests) == 3
        assert MockDownloader.requests[0].startswith('https://office1.ru/modules.php?name=sud_delo&adm_case')
        assert MockDownloader.requests[1].startswith('https://office1.ru/next_page_a')
        assert MockDownloader.requests[2].startswith('https://office1.ru/next_page_b')


class TestTimer:
    """Таймер."""

    def test_all(self) -> None:
        """Проверка всех методов таймера."""
        timer = Timer()
        assert timer.is_ended() is True
        timer.set_after(10)
        assert timer.seconds_left() == 10
        assert timer.is_ended() is False
