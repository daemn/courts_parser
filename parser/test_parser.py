import pytest
from parser import OfficesParser, ParserError, read_date, AdmCasesParser, CivCasesParser
from datetime import datetime
import os


def load_page(file_name: str) -> bytes:
    """
    Загружает тестовую страницу из файла.

    :param file_name: Имя файла страницы.
    :return: Содержимое тестовой страницы.
    """
    with open(os.path.join('test_pages', file_name), 'rb') as fd:
        return fd.read()


class TestOffices:
    """Поиск судебных участков на странице."""

    def test_empty_page(self) -> None:
        """Пустая страница должна вызывать исключение ParserError."""
        page: bytes = load_page('empty.html')
        with pytest.raises(ParserError):
            parser = OfficesParser(page)
            list(parser.offices())

    def test_no_case_page(self) -> None:
        """Страница без судебных участков должна вызывать исключение ParserError."""
        page: bytes = load_page('offices.no_cases.html')
        with pytest.raises(ParserError):
            parser = OfficesParser(page)
            list(parser.offices())

    def test_bad_page(self) -> None:
        """'Плохая' страница должна вызывать исключение ParserError."""
        page: bytes = load_page('offices.bad.html')
        with pytest.raises(ParserError):
            parser = OfficesParser(page)
            list(parser.offices())

    def test_good_page(self) -> None:
        """Обычная страница со списком судебных участков."""
        page: bytes = load_page('offices.html')
        parser = OfficesParser(page)
        offices = list(parser.offices())
        for office in offices:
            assert isinstance(office, dict)
            assert isinstance(office['name'], str)
            assert isinstance(office['url'], str)
            assert len(office['name']) > 0
            assert office['url'].startswith('http')
        assert len(offices) == 185
        assert offices[2]['name'] == 'Судебный участок №3 Советского района г.Челябинска'
        assert offices[2]['url'] == 'http://sovchel3.chel.msudrf.ru'


class TestBadPages:
    """Реакция на ошибочные страницы парсера судебных дел."""

    def test_empty_page(self) -> None:
        """Пустая страница вызывает исключение ParserError."""
        page: bytes = load_page('empty.html')

        with pytest.raises(ParserError):
            parser = AdmCasesParser(page)
            list(parser.cases())

        with pytest.raises(ParserError):
            parser = CivCasesParser(page)
            list(parser.cases())

    def test_bad_page(self) -> None:
        """'Плохая' страница вызывает исключение ParserError."""
        page: bytes = load_page('cases.bad.html')

        with pytest.raises(ParserError):
            parser = AdmCasesParser(page)
            list(parser.cases())

        with pytest.raises(ParserError):
            parser = CivCasesParser(page)
            list(parser.cases())


class TestCivilCases:
    """Поиск гражданских дел."""

    def test_not_found(self) -> None:
        """Нормальная страница с пустым результатом поиска возвращает пустой список."""
        page: bytes = load_page('cases.civ.not_found.html')
        parser = CivCasesParser(page)
        cases = list(parser.cases())
        assert cases == []

    def test_one_page(self) -> None:
        """Нормальная страница с делами на одной странице возвращает список дел."""
        page: bytes = load_page('cases.civ.one_page.html')
        parser = CivCasesParser(page)
        cases = list(parser.cases())
        assert len(cases) == 2
        assert cases[0] == {
            'number':       '2-4615/2021',
            'url':          '/modules.php?name=sud_delo&op=cs&case_id=4475347&delo_id=1540005',
            'category':     'Прочие исковые дела Иные исковые дела',
            'plaintiff':    'ГСК № 202',
            'defendant':    'Криволапов Юрий Николаевич',
            'date':         datetime(day=21, month=10, year=2021),
            'judgment':     'Судебный приказ',
            'code':         'civ',
        }

    def test_first_page(self) -> None:
        """Нормальная страница с делами на нескольких страницах возвращает список дел."""
        page: bytes = load_page('cases.civ.first_page.html')
        parser = CivCasesParser(page)
        cases = list(parser.cases())
        assert len(cases) == 25
        assert cases[2] == {
            'number':       '2-4612/2021',
            'url':          '/modules.php?name=sud_delo&op=cs&case_id=4475334&delo_id=1540005',
            'category':     ('Споры, возникающие из семейных правоотношений О взыскании алиментов'
                             ' на содержание несовершеннолетних детей'),
            'plaintiff':    'Опарова Дарья Александровна',
            'defendant':    'Новиков Константин Валерьевич',
            'date':         datetime(day=20, month=10, year=2021),
            'judgment':     'Судебный приказ',
            'code': 'civ',
        }


class TestAdministrativeCases:
    """Поиск административных дел."""

    def test_not_found(self) -> None:
        """Нормальная страница с пустым результатом поиска возвращает пустой список."""
        page: bytes = load_page('cases.adm.not_found.html')
        parser = AdmCasesParser(page)
        cases = list(parser.cases())
        assert cases == []

    def test_one_page(self) -> None:
        """Нормальная страница с делами на одной странице возвращает список дел."""
        page: bytes = load_page('cases.adm.one_page.html')
        parser = AdmCasesParser(page)
        cases = list(parser.cases())
        assert len(cases) == 6
        assert cases[0] == {
            'number':       '3-636/2021',
            'url':          '/modules.php?name=sud_delo&op=cs&case_id=4475466&delo_id=1500001',
            'defendant':    'Бабанов Александр Иванович',
            'article':      'ст. 7.27 ч. 1',
            'date':         datetime(day=20, month=10, year=2021),
            'judgment':     'Постановление о назначении административного наказания',
            'code':         'adm',
        }

    def test_first_page(self) -> None:
        """Нормальная страница с делами на нескольких страницах возвращает список дел."""
        page: bytes = load_page('cases.adm.first_page.html')
        parser = AdmCasesParser(page)
        cases = list(parser.cases())
        assert len(cases) == 25
        assert cases[2] == {
            'number':       '3-630/2021',
            'url':          '/modules.php?name=sud_delo&op=cs&case_id=4473936&delo_id=1500001',
            'defendant':    'Евстаров Дмитрий Вадимович',
            'article':      'ст. 20.25 ч. 1',
            'date':         datetime(day=15, month=10, year=2021),
            'judgment':     'Постановление о назначении административного наказания',
            'code':         'adm',
        }

    def test_broken_page(self) -> None:
        """Сломанная страница без элементов кодекса с делами на двух страницах возвращает список дел."""
        page: bytes = load_page('cases.adm.no_code.html')
        parser = AdmCasesParser(page)
        cases = list(parser.cases())
        assert len(cases) == 25
        assert cases[1] == {
            'number':       '3-47/2021',
            'url':          '/modules.php?name=sud_delo&op=cs&case_id=527733&delo_id=1500001',
            'defendant':    'Юшков Максим Георгиевич',
            'article':      'ст. 6.9 ч. 1',
            'date':         datetime(day=10, month=1, year=2021),
            'judgment':     'Постановление о назначении административного наказания',
            'code':         'adm',
        }


class TestUrlToNextCasesPage:
    """Поиск url на следующую страницу со списком дел."""

    def test_not_found(self) -> None:
        """Обычная страница. Дела не найдены. Ссылки нет."""
        page: bytes = load_page('cases.adm.not_found.html')

        parser = AdmCasesParser(page)
        assert parser.url_to_next_page() == ''

        parser = CivCasesParser(page)
        assert parser.url_to_next_page() == ''

    def test_one_page(self) -> None:
        """Обычная страница. Все дела на одной странице. Ссылки нет."""
        page: bytes = load_page('cases.adm.one_page.html')

        parser = AdmCasesParser(page)
        assert parser.url_to_next_page() == ''

        parser = CivCasesParser(page)
        assert parser.url_to_next_page() == ''

    def test_first_page(self) -> None:
        """Первая страница из большого списка. Возвращается ссылка на вторую."""
        page: bytes = load_page('cases.adm.first_page.html')

        parser = AdmCasesParser(page)
        assert parser.url_to_next_page() == (
            '/modules.php?name=sud_delo&adm_case__RESULT_DATE1D=01.02.2021&adm_case__RESULT_DATE2D=24.10.2021'
            '&delo_id=1500001&op=sf&pageNum_Recordset1=1'
        )

        parser = CivCasesParser(page)
        assert parser.url_to_next_page() == (
            '/modules.php?name=sud_delo&adm_case__RESULT_DATE1D=01.02.2021&adm_case__RESULT_DATE2D=24.10.2021'
            '&delo_id=1500001&op=sf&pageNum_Recordset1=1'
        )

    def test_second_page(self) -> None:
        """Вторая страница из большого списка. Возвращается ссылка на третью."""
        page: bytes = load_page('cases.adm.second_page.html')

        parser = AdmCasesParser(page)
        assert parser.url_to_next_page() == (
            '/modules.php?name=sud_delo&adm_case__RESULT_DATE1D=01.02.2021&adm_case__RESULT_DATE2D=24.10.2021'
            '&delo_id=1500001&op=sf&pageNum_Recordset1=2'
        )

        parser = CivCasesParser(page)
        assert parser.url_to_next_page() == (
            '/modules.php?name=sud_delo&adm_case__RESULT_DATE1D=01.02.2021&adm_case__RESULT_DATE2D=24.10.2021'
            '&delo_id=1500001&op=sf&pageNum_Recordset1=2'
        )

    def test_prelast_page(self) -> None:
        """Предпоследняя страница из большого списка. Возвращается ссылка на последнюю."""
        page: bytes = load_page('cases.adm.prelast_page.html')

        parser = AdmCasesParser(page)
        assert parser.url_to_next_page() == (
            '/modules.php?name=sud_delo&adm_case__RESULT_DATE1D=01.02.2021&adm_case__RESULT_DATE2D=24.10.2021'
            '&delo_id=1500001&op=sf&pageNum_Recordset1=22'
        )

        parser = CivCasesParser(page)
        assert parser.url_to_next_page() == (
            '/modules.php?name=sud_delo&adm_case__RESULT_DATE1D=01.02.2021&adm_case__RESULT_DATE2D=24.10.2021'
            '&delo_id=1500001&op=sf&pageNum_Recordset1=22'
        )

    def test_last_page(self) -> None:
        """Последняя страница из большого списка. Ссылки на следующую нет."""
        page: bytes = load_page('cases.adm.last_page.html')

        parser = AdmCasesParser(page)
        assert parser.url_to_next_page() == ''

        parser = CivCasesParser(page)
        assert parser.url_to_next_page() == ''


class TestReadDate:
    """Чтение даты в строке."""

    def test_correct_dates(self) -> None:
        """Чтение дат в допустимых форматах."""
        target_date = datetime(2012, 1, 3)
        assert read_date('**2012/1/3___') == target_date
        assert read_date('2012.1.3') == target_date
        assert read_date('3,1,2012') == target_date
        assert read_date('(----3-1-2012)') == target_date

    def test_incorrect_dates(self) -> None:
        """Исключение ValueError при чтении некорректных дат."""
        with pytest.raises(ValueError):
            read_date('')
        with pytest.raises(ValueError):
            read_date('234kkk')
