#!/bin/env python3

import os
import logging as log
import time
from datetime import datetime, timedelta
import urllib3
from config import app_config
from parser import OfficesParser, ParserError, AdmCasesParser, CivCasesParser
from case_storage import CaseWebStorage, CaseStorageError
from typing import List, Optional, Iterator
import abc

__doc__ = """Парсер судебных приказов.
Программа периодически ищет новые судебные дела на сайтах судебных участков и отправляет в хранилище."""

log.basicConfig(level=log.getLevelName(app_config['log_level']), format='%(levelname)s : %(filename)s : %(message)s')


class BadPagesStorage:
    """Хранилище "плохих" web-страниц.

    >>> BadPagesStorage.use_dir('/some/dir')
    >>> stor = BadPagesStorage('prefix')
    >>> stor.save(b'html page')
    """

    max_files: int = 10  # Максимальное количество файлов в хранилище.
    path: str = ''       # Путь к хранилищу.

    @classmethod
    def use_dir(cls, path: str) -> None:
        """Указывает директорию для хранилища. Создаёт директорию, если её нет.

        :param path: Путь к хранилищу.
        """
        cls.path = path
        if path:
            log.debug(f'New storage dir is "{path}".')
            os.makedirs(path, exist_ok=True)

    prefix: str  # Префикс имён файлов.

    def __init__(self, prefix: str = '') -> None:
        """
        :param prefix: Префикс имени файла.
        """
        self.prefix = prefix

    def save(self, page: bytes) -> None:
        """Сохраняет страницу в хранилище. Если путь к хранилищу пуст, то метод 'save' не сохраняет ничего.

        :param page: Страница для сохранения.
        """
        path = self.__class__.path
        if not path:
            return
        file_name: str = f'{self.prefix}page_{hash(page)}.html'
        file_path: str = os.path.join(path, file_name)
        log.info(f'Try to save "bad page" as "{file_name}".')
        with open(file_path, 'wb') as file:
            file.write(page)
        self._check_storage_size()

    def _check_storage_size(self) -> None:
        path = self.__class__.path
        if not path:
            return
        max_files = self.__class__.max_files

        file_names: List[str] = os.listdir(path)
        if len(file_names) < max_files:
            return

        file_paths: List[str] = list(os.path.join(path, x) for x in file_names)
        file_paths.sort(key=lambda x: os.stat(x).st_ctime)
        for file_path in file_paths[:-max_files]:
            os.remove(file_path)


class DownloadError(Exception):
    """Ошибка скачивания страницы."""
    pass


class ABCDownloader(abc.ABC):
    """Базовый класс скачивателя страниц."""

    status: int     # HTTP-код ответа.

    def __init__(self) -> None:
        self.headers = {}
        self.status = 0

    @abc.abstractmethod
    def get(self, url: str) -> bytes:
        """Скачивает страницу."""
        pass


class WebDownloader(ABCDownloader):
    """Скачиватель страниц.

    >>> download = WebDownloader()
    >>> page = download.get('http://url.com')
    >>> download.status  # 200
    """

    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) '
                      'Chrome/92.0.4515.107 Safari/537.36'
    }
    _pool_manager = urllib3.PoolManager(cert_reqs='CERT_NONE')

    def get(self, url: str) -> bytes:
        """Скачивает страницу.

        :param url: Ссылка на web-страницу.
        :return: Содержимое запрошенной страницы.
        :raise DownloadError: Ошибка загрузки страницы.
        """
        self.status = 0
        log.info(f'Try to get web page "{url}".')

        # todo: Добавить таймауты и попытки получения страницы.
        try:
            http_request = self.__class__._pool_manager.request(
                method='get', url=url, headers=self.__class__.headers
            )
        except urllib3.exceptions.HTTPError as err:
            raise DownloadError(f'Web request failed ({url}).') from err

        self.status = http_request.status
        if self.status != 200:
            raise DownloadError(f'Web service return error {self.status} ({url}).')

        return http_request.data


class CheckerError(Exception):
    """Ошибка чекера судебных дел или судебных участков."""
    pass


class CheckerDownloadError(CheckerError):
    """Ошибка чекера. Ошибка загрузки страницы."""
    pass


class CheckerParseError(CheckerError):
    """Ошибка чекера. Ошибка парсинга страницы."""
    pass


class ABCOfficeChecker(abc.ABC):
    """Чекер судебного участка. Базовый класс."""

    code: str = ''  # Кодекс искомых судебных дел.
    search_cases_age: int = 30  # Максимальный возраст искомых судебных дел.
    search_cases_days_limit: int = 10  # Ограничение количества дней для запроса судебных дел.

    name: str  # Имя судебного участка.
    url: str   # Ссылка на сайт судебного участка.

    def __init__(self, name: str, url: str) -> None:
        """
        :param name: Имя судебного участка.
        :param url: Ссылка на сайт судебного участка.
        """
        self.name = name
        self.url = url
        self._downloader = WebDownloader()
        self._bad_storage = BadPagesStorage('cases_')
        self._storage = CaseWebStorage()
        self._parser = None

    @abc.abstractmethod
    def _assemble_search_url(self, from_date: datetime, to_date: datetime) -> str:
        # Обращаю внимание, что поисковые ссылки для разных типов документов сильно отличаются. И значение параметра
        # "delo_id", и имена параметров диапазона дат, одновременно указывают на тип искомых документов. Поэтому для
        # каждого типа судебных дел (гражданских, административных) нужен свой метод формирования поисковой ссылки.
        pass

    def _calculate_date_range(self) -> tuple:
        """Рассчитывает диапазон дат для поиска судебных дел.

        :return: tuple(from_date, to_date) Диапазон дат.
        """
        today: datetime = datetime.utcnow()
        max_case_date: datetime = self._storage.get_max_case_date(office=self.name, code=self.code)
        from_date_min_border: datetime = today - timedelta(days=self.search_cases_age)
        from_date: datetime = min(max(max_case_date or from_date_min_border, from_date_min_border), today)
        to_date: datetime = min(today, from_date + timedelta(days=self.search_cases_days_limit))
        return from_date, to_date

    def grab_cases(self) -> None:
        """Ищет новые дела на сайте и сохраняет в хранилище.

        :raise CheckerError: Ошибка при проверке новых судебных дел.
        """
        from_date, to_date = self._calculate_date_range()
        # Чтобы перечитать все страницы с судебными делами, сначала запрашивается страница по сформированной ссылке,
        # затем на полученной странице ищется ссылка на следующую страницу, и так далее.
        search_url: str = self._assemble_search_url(from_date=from_date, to_date=to_date)
        max_cases_date: datetime = from_date
        try:
            while search_url:
                page: bytes = self._downloader.get(self.url + search_url)
                parser = self._parser(page)
                for case in parser.cases():
                    case['url']: str = self.url + case['url']
                    case['office']: str = self.name
                    self._storage.save_case(case)
                    if 'date' in case:
                        max_cases_date = max(max_cases_date, case['date'])
                search_url: str = parser.url_to_next_page()
        except DownloadError as err:
            raise CheckerDownloadError('Cases page download failed.') from err
        except ParserError as err:
            self._bad_storage.save(err.page)
            raise CheckerParseError('Read cases failed.') from err
        self._storage.set_max_case_date(office=self.name, code=self.code, max_date=max_cases_date)


class OfficeCheckerAdm(ABCOfficeChecker):
    """Чекер административных дел.

    >>> office = OfficeCheckerAdm(name='office_1', url='http://office.url')
    >>> office.grab_cases()
    """

    code = 'adm'

    def __init__(self, name: str, url: str) -> None:
        """
        :param name: Имя судебного участка.
        :param url: Ссылка на сайт судебного участка.
        """
        super().__init__(name, url)
        self._parser = AdmCasesParser

    def _assemble_search_url(self, from_date: datetime, to_date: datetime) -> str:
        return (
            '/modules.php?name=sud_delo'
            '&adm_case__RESULT_DATE1D={}'
            '&adm_case__RESULT_DATE2D={}'
            '&delo_id=1500001'
            '&op=sf'
            .format(from_date.strftime('%d.%m.%Y'), to_date.strftime('%d.%m.%Y'))
        )


class OfficeCheckerCiv(ABCOfficeChecker):
    """Чекер гражданских дел.

    >>> office = OfficeCheckerCiv(name='office_1', url='http://office.url')
    >>> office.grab_cases()
    """

    code = 'civ'

    def __init__(self, name: str, url: str) -> None:
        """
        :param name: Имя судебного участка.
        :param url: Ссылка на сайт судебного участка.
        """
        super().__init__(name, url)
        self._parser = CivCasesParser

    def _assemble_search_url(self, from_date: datetime, to_date: datetime) -> str:
        return (
            '/modules.php?name=sud_delo'
            '&g1_case__RESULT_DATE1D={}'
            '&g1_case__RESULT_DATE2D={}'
            '&delo_id=1540005'
            '&op=sf'
            .format(from_date.strftime('%d.%m.%Y'), to_date.strftime('%d.%m.%Y'))
        )


class OfficeCheckersDispatcher:
    """Диспетчер чекеров судебных участков."""

    search_offices_period: int = 60
    search_offices_fail_sleep: int = 60
    search_cases_period: int = 60
    search_cases_fail_sleep: int = 60

    url: str
    jobs: list

    def __init__(self, url: str) -> None:
        """
        :param url: Ссылка на страницу со списком судебных участков.
        """
        self.url = url
        self.jobs = []
        self._reassemble_list_at = Timer()
        self._downloader = WebDownloader()
        self._bad_storage = BadPagesStorage('offices_')

    def _assemble_jobs_list(self) -> None:
        """Проверяет список судебных участков на сайте. И собирает их в список задач.

        :raise CheckerError: 'Плохая' web-страница или другие проблемы с чтением.
        """
        search_offices_fail_sleep: int = self.search_offices_fail_sleep
        while True:
            try:
                page: bytes = self._downloader.get(self.url)
                parser = OfficesParser(page)

                new_jobs = []
                for office in parser.offices():
                    for checker_class_ in OfficeCheckerCiv, OfficeCheckerAdm:
                        checker: ABCOfficeChecker = checker_class_(**office)
                        check_time = Timer()
                        new_jobs.append({'checker': checker, 'time': check_time})
                self.jobs = new_jobs

            except DownloadError as err:
                log.error(f'Offices page download failed. {err}')

            except ParserError as err:
                self._bad_storage.save(err.page)
                log.error(f'Offices page parse failed. {err}')

            if self.jobs:
                return
            else:
                time.sleep(search_offices_fail_sleep)
                search_offices_fail_sleep += self.search_offices_fail_sleep

    def run(self) -> None:
        """Выполняет задачи по очереди. Не возвращает управление."""
        while True:

            # Если пора, то прочитать список судебных участков.
            if self._reassemble_list_at.is_ended():
                self._assemble_jobs_list()
                self._reassemble_list_at.set_after(self.search_offices_period)

            # Исполнить следующую задачу.
            job = self._find_next_job()
            try:
                job['time'].wait()
                job['checker'].grab_cases()
                job['time'].set_after(self.search_cases_period)
            except CheckerError as err:
                log.error(str(err))
                job['time'].set_after(self.search_cases_fail_sleep)

    def _find_next_job(self) -> dict:
        result: dict = self.jobs[0]
        for job in self.jobs:
            if job['time'].seconds_left() < result['time'].seconds_left():
                result = job
        return result


class Timer:
    """Таймер.

    >>> tea_time = Timer()
    >>> tea_time.set_after(10)
    >>> time.sleep(6)
    >>> tea_time.seconds_left()  # 4
    >>> tea_time.is_ended()      # False
    """

    next_time: int

    def __init__(self) -> None:
        self.next_time = 0

    def set_after(self, seconds: int) -> None:
        """Установить таймер через указанное время.

        :param seconds: Через сколько секунд закончиться.
        """
        self.next_time = int(time.monotonic()) + seconds

    def wait(self) -> None:
        """Дождаться окончания таймера."""
        time.sleep(self.seconds_left())

    def seconds_left(self) -> int:
        """Сколько секунд осталось ждать,

        :return: Количество секунд.
        """
        seconds: int = self.next_time - int(time.monotonic())
        return seconds if seconds > 0 else 0

    def is_ended(self) -> bool:
        """Таймер закончился?"""
        return int(time.monotonic()) >= self.next_time


def main() -> None:
    BadPagesStorage.max_files = app_config['bad_pages_max']
    BadPagesStorage.use_dir(app_config['bad_pages_path'])

    CaseWebStorage.server_url = app_config['storage_url']

    OfficeCheckerCiv.search_cases_age = app_config['search_cases_age']
    OfficeCheckerCiv.search_cases_days_limit = app_config['search_cases_days_limit']
    OfficeCheckerAdm.search_cases_age = app_config['search_cases_age']
    OfficeCheckerAdm.search_cases_days_limit = app_config['search_cases_days_limit']

    OfficeCheckersDispatcher.search_offices_period = app_config['search_offices_period']
    OfficeCheckersDispatcher.search_offices_fail_sleep = app_config['search_offices_fail_sleep']
    OfficeCheckersDispatcher.search_cases_period = app_config['search_cases_period']
    OfficeCheckersDispatcher.search_cases_fail_sleep = app_config['search_cases_fail_sleep']

    dispatcher = OfficeCheckersDispatcher(app_config['offices_url'])
    dispatcher.run()


if __name__ == '__main__':
    main()
