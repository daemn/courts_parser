import os

__doc__: str = """Конфигурация программы.
Все опции, перечисленные здесь, можно переопределить переменными окружения.
Например опция log_level соответствует переменной COURTS_LOG_LEVEL.
"""

default: tuple = (
    # Уровень логирования.
    ('log_level', 'WARNING', lambda a: a.upper()),
    # URL страницы со списком судебных участков.
    ('offices_url', 'https://just.gov74.ru/gyu/Dop/Obespechenie/Sait.htm', None),
    # Период в секундах обновления списка участков.
    ('search_offices_period', 60 * 60 * 5, int),
    # Задержка между попытками обновления списка участков в случае ошибки.
    ('search_offices_fail_sleep', 60, int),
    # Искать судебные дела не старше этого срока в днях.
    ('search_cases_age', 30, int),
    # Запрашивать дела за период не больше этого в днях.
    ('search_cases_days_limit', 10, int),
    # Период в секундах получения данных с участков.
    ('search_cases_period', 60 * 60 * 1, int),
    # Пауза в секундах получения данных с участков в случае ошибки.
    ('search_cases_fail_sleep', 60 * 60 * 1, int),
    # Путь сохранения 'плохих' страниц.
    ('bad_pages_path', os.path.join(os.sep, 'var', 'bad_pages'), None),
    # Максимальное количество 'плохих' страниц в хранилище.
    ('bad_pages_max', 30, int),
    # Адрес хранилища судебных дел.
    ('storage_url', 'http://localhost:80', None),
)
app_config: dict = {}
for key, value, func in default:
    app_config[key] = os.getenv('COURTS_' + key.upper(), value)
    if func is not None:
        app_config[key] = func(app_config[key])

if __name__ == '__main__':
    from pprint import pprint
    pprint(app_config)
