import abc
import bs4.element
from bs4 import BeautifulSoup
import re
import logging as log
from config import app_config
from typing import List, Tuple, Iterator, Optional
from datetime import datetime

__doc__ = """Библиотека парсера html-страниц с судебными приказами."""

log.basicConfig(level=log.getLevelName(app_config['log_level']), format='%(levelname)s : %(filename)s : %(message)s')


class ParserError(Exception):
    """Ошибка парсинга веб-страницы."""

    page: bytes  # Содержимое плохой страницы.

    def __init__(self, message: str = None, page: bytes = b'') -> None:
        """
        :param message: Сообщение об ошибке.
        :param page: Содержимое страницы.
        """
        if message is None:
            message = 'Trying to parse web-page was failed.'
        self.page = page
        super().__init__(message)


class ABCOfficesParser(abc.ABC):
    """Базовый класс парсера списка судебных участков."""

    _page: bytes

    def __init__(self, page: bytes) -> None:
        """:param page: Страница со списком участков."""
        self._page = page

    @abc.abstractmethod
    def offices(self) -> Iterator:
        """Возвращает словари с именем и url участка."""
        pass


class OfficesParser(ABCOfficesParser):
    """Парсер списка судебных участков.
    >>> parser = OfficesParser(b'some valid page')
    >>> list(parser.offices())  # [{'name': 'office1', 'url': 'http://...'}, ... ]
    """

    _soup: BeautifulSoup

    def __init__(self, page: bytes) -> None:
        """Читает страницу со списком судебных участков.

        :param page: Страница со списком участков.
        :raise ParserError: Проблемы с чтением страницы.
        """
        super().__init__(page)
        try:
            self._soup = BeautifulSoup(page, 'html.parser')
        except Exception as err:
            # todo: Заменить на нормальный эксепшен
            raise ParserError('The offices soup was not cooked.', page=page) from err

    def offices(self) -> Iterator:
        """Возвращает словари с именем и url участка.

        Пример найденного участка:
        {"name": "Судебный участок 0", "url": "http://somewere"}

        :return: Судебный участок.
        :raise ParserError: Проблемы с чтением страницы.
        """
        # Таблицу со списком судебных участков можно найти по тегу 'table' и тексту 'Судебный участок' в полях.
        for tag_table in self._soup.find_all('table'):
            if tag_table.find(lambda x: x.text.startswith('Судебный участок')):
                break
        else:
            raise ParserError('An offices table was not found.', page=self._page)

        for tag_tr in tag_table.find_all('tr'):

            # Если в поле нет тега 'a', то это не ссылка на участок.
            tag_a: bs4.element.Tag = tag_tr.find('a')
            if not tag_a:
                continue

            # Текст ссылки -- это название участка.
            office_name: str = str(tag_a.text).strip()
            if not office_name:
                log.error(f'Office {str(tag_a.contents)} haven`t a name.')
                continue

            # Адрес ссылки -- это url участка.
            office_url: str = str(tag_a.get('href', '')).strip().rstrip('/')
            if not office_url.startswith('http'):
                log.error(f'The Office "{office_name}" have a wrong url "{office_url}".')
                continue

            yield {'name': office_name, 'url': office_url}


def read_date(string: str) -> datetime:
    """Читает в строке дату и возвращает её в виде datetime-объекта.

    :param string: Строка, содержащая дату.
    :return: Дата.
    :raise ValueError: Нечитабельный формат даты.
    """
    for writing_option in (
        r'(?P<year>[0-9]{4}).(?P<month>[0-9]{1,2}).(?P<day>[0-9]{1,2})',
        r'(?P<day>[0-9]{1,2}).(?P<month>[0-9]{1,2}).(?P<year>[0-9]{4})',
        r'(?P<day>[0-9]{1,2}).(?P<month>[0-9]{1,2}).(?P<year>[0-9]{2})',
    ):
        match = re.search(writing_option, string)
        if match:
            break
    else:
        raise ValueError('The string is not like a date string.')
    date_tuple: Tuple[int] = tuple(map(int, (
        match.group('year'), match.group('month'), match.group('day')
    )))
    return datetime(*date_tuple)


class ABCCasesParser(abc.ABC):
    """Базовый класс парсера судебных дел."""

    _page: bytes

    def __init__(self, page: bytes) -> None:
        """:param page: Страница со списком дел."""
        self._page = page

    @abc.abstractmethod
    def cases(self) -> Iterator:
        """Возвращает судебные дела по одному."""
        pass

    @abc.abstractmethod
    def url_to_next_page(self) -> str:
        """Возвращает ссылку на следующую страницу с судебными делами."""
        pass


class CasesParser(ABCCasesParser):
    """Парсер судебных дел. Фундаментальный класс."""

    _soup: BeautifulSoup

    def __init__(self, page: bytes) -> None:
        """
        :param page: Страница со списком дел.
        :raise ParserError: Проблемы с чтением страницы.
        """
        super().__init__(page)
        try:
            self._soup = BeautifulSoup(page, 'html.parser')
        except Exception as err:
            # todo: Заменить на нормальный эксепшен
            raise ParserError('The cases soup was not cooked.', page=page) from err

    def cases(self) -> Iterator:
        """Возвращает судебные дела по одному.

        :raise ParserError: Проблемы с парсингом страницы.
        """
        tag_table: bs4.element.Tag = self._cases_table()
        if tag_table is None:
            return None

        for case in self._cases(tag_table):
            yield case

    def _cases_table(self) -> Optional[bs4.element.Tag]:
        # Таблицу судебных дел можно найти по тегу 'table' и id 'tablcont'.
        tag_table: bs4.element.Tag = self._soup.find('table', id='tablcont')
        if tag_table is None:
            if self._soup.find('div', class_='search-error'):
                # Не найдена таблица с делами, потому что нет подходящих дел.
                return None
            else:
                # Не найдена таблица с делами, потому что страница "плохая".
                raise ParserError('No cases table was found.', page=self._page)
        return tag_table

    @abc.abstractmethod
    def _cases(self, container: bs4.element.Tag) -> Iterator:
        pass

    def url_to_next_page(self) -> str:
        """Возвращает ссылку на следующую страницу с судебными делами.

        :return: Ссылка не следующую страницу или ''.
        :raise ParserError: Проблемы с парсингом страницы.
        """
        # div с количеством найденных дел можно найти по id 'search_results'.
        tag_div: bs4.element.Tag = self._soup.find('div', id='search_results')
        if tag_div is None:
            raise ParserError('Wrong page. Search results not found.', page=self._page)
        if tag_div.find(string=re.compile('Данных по запросу не найдено')):
            # Дел не найдено, соответственно ссылки тоже нет.
            return ''
        tag_span: bs4.element.Tag = tag_div.find('span')
        if tag_span is None:
            # Страница всего одна. Ссылки нет.
            return ''
        li = tag_span.parent.nextSibling
        if li is None:
            # Это последняя страница. Ссылки нет.
            return ''

        return '/' + li.a['href'].strip().lstrip('/')


class CivCasesParser(CasesParser):
    """Парсер гражданских судебных дел.

    Пример гражданского дела:
    {
      'number': '2-3790/2021',              # Номер дела на сайте.
      'url': '/modules.php?name=sud...',    # Ссылка на дело.
      'category': 'Споры, связанные...',    # Категория дела.
      'judge': 'Бесстыжий',                 # Судья.
      'plaintiff': 'ПАО "Сбербанк"',        # Истец.
      'defendant': 'Любимова Лилиия...',    # Ответчик.
      'date': '28.07.2021',                 # Дата решения.
      'judgment': 'Судебный приказ',        # Судебное решение.
      'code': 'civ',                        # Кодекс гражданский.
    }

    >>> parser = CivCasesParser(b'some valid page')
    >>> list(parser.cases())  # [{'number': '8/a5', 'url': 'http://...', ... }, ... ]
    """

    def _cases(self, container: bs4.element.Tag) -> Iterator:
        """Возвращает судебные дела по одному.

        :raise ParserError: Проблемы с парсингом страницы.
        """
        for tag_tr in container.find_all('tr'):

            # Строка таблицы с тегом 'th' -- это заголовок. Игнорировать.
            if tag_tr.find('th'):
                continue

            case = {}

            # Первое поле в строке содержит url на подробности и номер дела.
            tag_td: bs4.element.Tag = tag_tr.find('td')
            case['url'] = '/' + str(tag_td.a['href']).strip().lstrip('/')
            case['number'] = str(tag_td.a.text).strip()
            if not case['number']:
                log.error(f'Case "{case["url"]}" have a wrong number.')
                continue

            # Обозначение кодекса ('civ', 'adm').
            case['code'] = 'civ'

            # В следующем поле категория, истец и ответчик.
            tag_td: bs4.element.Tag = tag_td.find_next_sibling()
            fields: Tuple[str] = tuple(str(x).strip() for x in tag_td.contents)
            for key, word in (
                ('category', 'КАТЕГОРИЯ:'),
                ('plaintiff', 'ИСТЕЦ:'),
                ('defendant', 'ОТВЕТЧИК:'),
            ):
                case[key] = ''
                for field in fields:
                    if field.startswith(word):
                        case[key] = field[len(word):].strip()

            # В следующем поле судья. Игнорировать.
            tag_td: bs4.element.Tag = tag_td.find_next_sibling()

            # В следующем поле дата решения.
            tag_td: bs4.element.Tag = tag_td.find_next_sibling()
            date_text: str = str(tag_td.text).strip()
            try:
                case['date']: datetime = read_date(date_text)
            except (TypeError, ValueError):
                log.error(f'Wrong format of date "{date_text}".')

            # В следующем поле вид решения.
            tag_td: bs4.element.Tag = tag_td.find_next_sibling()
            case['judgment'] = str(tag_td.text).strip()

            yield case


class AdmCasesParser(CasesParser):
    """Парсер административных судебных дел.

    Пример административного дела:
    {
      'number': '2-3790/2021',              # Номер дела на сайте.
      'defendant': 'Любимова Лилиия...',    # Ответчик.
      'article': 'ст. 6.9 ч. 1'             # Статья.
      'date': '28.07.2021',                 # Дата решения.
      'judgment': 'Судебный приказ',        # Судебное решение.
      'url': '/modules.php?name=sud...',    # Ссылка на дело.
      'code': 'adm',                        # Кодекс административный.
    }

    >>> parser = AdmCasesParser(b'some valid page')
    >>> list(parser.cases())  # [{'number': '8/a5', 'url': 'http://...', ... }, ... ]
    """

    def _cases(self, container: bs4.element.Tag) -> Iterator:
        """Возвращает судебные дела по одному.

        :raise ParserError: Проблемы с парсингом страницы.
        """
        for tag_tr in container.find_all('tr'):

            # Строка таблицы с тегом 'th' -- это заголовок. Игнорировать.
            if tag_tr.find('th'):
                continue

            case = {}

            # Первое поле в строке содержит url на подробности и номер дела.
            tag_td: bs4.element.Tag = tag_tr.find('td')
            case['url'] = '/' + str(tag_td.a['href']).strip().lstrip('/')
            case['number'] = str(tag_td.a.text).strip()
            if not case['number']:
                log.error(f'Case "{case["url"]}" have a wrong number.')
                continue

            # Обозначение кодекса ('civ', 'adm').
            case['code'] = 'adm'

            # В следующем поле ответчик и статья.
            tag_td: bs4.element.Tag = tag_td.find_next_sibling()
            fields: List[str] = tag_td.text.split('-')
            case['defendant']: str = fields[0].strip()
            if len(fields) > 1:
                case['article']: str = fields[-1].strip().rstrip(';')

            # В следующем поле судья. Игнорировать.
            tag_td: bs4.element.Tag = tag_td.find_next_sibling()

            # В следующем поле дата решения.
            tag_td: bs4.element.Tag = tag_td.find_next_sibling()
            date_text: str = str(tag_td.text).strip()
            try:
                case['date']: datetime = read_date(date_text)
            except (TypeError, ValueError):
                log.error(f'Wrong format of date "{date_text}".')

            # В следующем поле вид решения.
            tag_td: bs4.element.Tag = tag_td.find_next_sibling()
            case['judgment'] = str(tag_td.text).strip()

            yield case
