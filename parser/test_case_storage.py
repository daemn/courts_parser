from typing import List
import json
from datetime import datetime
import case_storage
from case_storage import CaseWebStorage, CaseStorageError
from requests.exceptions import RequestException
import pytest


def new_test_civ_case(postfix: str) -> dict:
    return {
        'number':    f'test/190{postfix}',
        'category':  f'Категория_{postfix}',
        'office':    f'Участок_{postfix}',
        'judge':     f'Судья_{postfix}',
        'judgment':  f'Решение_{postfix}',
        'date':      datetime(2021, 7, 15),
        'plaintiff': f'Истец_{postfix}',
        'defendant': f'Ответчик_{postfix}',
        'url':       f'http://case{postfix}',
        'code':      'civ',
    }


def new_test_adm_case(postfix: str) -> dict:
    return {
        'number':    f'test/290{postfix}',
        'office':    f'Участок_{postfix}',
        'judge':     f'Судья_{postfix}',
        'judgment':  f'Решение_{postfix}',
        'date':      datetime(2021, 7, 15),
        'defendant': f'Ответчик_{postfix}',
        'article':   f'Статья_{postfix}',
        'url':       f'http://case{postfix}',
        'code':      'adm',
    }


def date_to_list(date_: datetime) -> list:
    """Переводит дату в список.

    :param date_: Дата.
    :return: Список вида: [год, месяц, день].
    """
    return [date_.year, date_.month, date_.day]


def list_to_date(date_: list) -> datetime:
    """Переводит список в дату.

    :param date_: Дата вида: [год, месяц, день].
    :return: Дата.
    """
    return datetime(date_[0], date_[1], date_[2])


class MockResponse:

    status_code: int = 200
    text: str = ''

    def __init__(self, status_code: int = 200, text: str = '') -> None:
        self.status_code = status_code
        self.text = text


class MockRequests:

    requests: list = []
    results: List = [None]

    def _get_result(self, **kwargs) -> MockResponse:
        self.requests.append(kwargs)
        result = self.results.pop(0)
        if result is None:
            raise RequestException
        return result

    def get(self, **kwargs) -> MockResponse:
        return self._get_result(**kwargs)

    def post(self, **kwargs) -> MockResponse:
        return self._get_result(**kwargs)


case_storage.requests = MockRequests()


class TestSaveCase:
    """Сохранение судебного дела в хранилище."""

    @pytest.fixture
    def storage(self) -> CaseWebStorage:
        return CaseWebStorage()

    @pytest.fixture
    def prepare_stage(self) -> None:
        MockRequests.requests = []
        MockRequests.results = [None]

    def test_empty(self, storage: CaseWebStorage) -> None:
        """Пустое дело."""
        with pytest.raises(CaseStorageError):
            storage.save_case({})

    def test_requests_fail(self, storage: CaseWebStorage, prepare_stage: None) -> None:
        """requests генерирует эксепшен."""
        case = new_test_adm_case('1')
        with pytest.raises(CaseStorageError):
            storage.save_case(case)

    def test_requests_error_404(self, storage: CaseWebStorage, prepare_stage: None) -> None:
        """requests возвращает http-код 404."""
        case = new_test_adm_case('1')
        MockRequests.results = [MockResponse(status_code=404)]

        with pytest.raises(CaseStorageError):
            storage.save_case(case)

        case['date'] = date_to_list(case['date'])
        assert MockRequests.requests == [{'url': '/case', 'json': {'case': case}}]

    def test_requests_error_502(self, storage: CaseWebStorage, prepare_stage: None) -> None:
        """requests возвращает http-код 502."""
        case = new_test_adm_case('1')
        MockRequests.results = [MockResponse(status_code=502)]

        with pytest.raises(CaseStorageError):
            storage.save_case(case)

    def test_adm_success(self, storage: CaseWebStorage, prepare_stage: None) -> None:
        """Нормальное административное дело."""
        case = new_test_adm_case('1')
        MockRequests.results = [MockResponse()]
        storage.save_case(case)

        case['date'] = date_to_list(case['date'])
        assert MockRequests.requests == [{'url': '/case', 'json': {'case': case}}]

    def test_civ_success(self, storage: CaseWebStorage, prepare_stage: None) -> None:
        """Нормальное гражданское дело."""
        case = new_test_civ_case('1')
        MockRequests.results = [MockResponse()]
        storage.save_case(case)

        case['date'] = date_to_list(case['date'])
        assert MockRequests.requests == [{'url': '/case', 'json': {'case': case}}]


class TestMaxCaseDate:
    """Максимальная дата судебного дела."""

    @pytest.fixture
    def storage(self) -> CaseWebStorage:
        return CaseWebStorage()

    @pytest.fixture
    def prepare_stage(self) -> None:
        MockRequests.requests = []
        MockRequests.results = [None]

    def test_requests_fail(self, storage: CaseWebStorage, prepare_stage: None) -> None:
        """requests генерирует эксепшен."""
        MockRequests.results = [None, None]

        with pytest.raises(CaseStorageError):
            storage.set_max_case_date(office='office1', code='adm', max_date=datetime(2022, 2, 19))

        with pytest.raises(CaseStorageError):
            storage.get_max_case_date(office='office1', code='adm')

    def test_requests_error_404(self, storage: CaseWebStorage, prepare_stage: None) -> None:
        """requests возвращает http-код 404."""
        MockRequests.results = [MockResponse(status_code=404), MockResponse(status_code=404)]

        with pytest.raises(CaseStorageError):
            storage.set_max_case_date(office='office1', code='adm', max_date=datetime(2022, 2, 19))

        with pytest.raises(CaseStorageError):
            storage.get_max_case_date(office='office1', code='adm')

    def test_requests_error_502(self, storage: CaseWebStorage, prepare_stage: None) -> None:
        """requests возвращает http-код 502."""
        MockRequests.results = [MockResponse(status_code=502), MockResponse(status_code=502)]

        with pytest.raises(CaseStorageError):
            storage.set_max_case_date(office='office1', code='adm', max_date=datetime(2022, 2, 19))

        with pytest.raises(CaseStorageError):
            storage.get_max_case_date(office='office1', code='adm')

    def test_adm_success(self, storage: CaseWebStorage, prepare_stage: None) -> None:
        """Нормальное административное дело."""
        MockRequests.results = [MockResponse(), MockResponse(text=json.dumps({'value': [2022, 2, 19]}))]

        storage.set_max_case_date(office='office1', code='adm', max_date=datetime(2022, 2, 19))
        max_case_date = storage.get_max_case_date(office='office1', code='adm')
        assert max_case_date == datetime(2022, 2, 19)

    def test_civ_success(self, storage: CaseWebStorage, prepare_stage: None) -> None:
        """Нормальное гражданское дело."""
        MockRequests.results = [MockResponse(), MockResponse(text=json.dumps({'value': [2022, 2, 19]}))]

        storage.set_max_case_date(office='office1', code='civ', max_date=datetime(2022, 2, 19))
        max_case_date = storage.get_max_case_date(office='office1', code='civ')
        assert max_case_date == datetime(2022, 2, 19)
