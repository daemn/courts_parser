#!/bin/env python3
from config import app_config
import logging as log
from flask import Flask, request, escape, render_template, url_for
import requests
import json
import math
import re

__doc__ = """Web страница проекта с формой поиска судебных дел."""

log.basicConfig(level=log.getLevelName(app_config['log_level']), format='%(levelname)s : %(filename)s : %(message)s')
app = Flask(__name__)


class SearchArgsError(Exception):
    """Ошибка в аргументах поиска судебных дел."""
    pass


class RequestError(Exception):
    """Общая ошибка при запросе к API."""
    pass


@app.route('/__doc__', methods=['GET'])
def get_doc() -> str:
    return f'<pre>{escape(__doc__)}</pre>'


@app.route('/', methods=['GET'])
def get_root() -> str:
    flash_message = ''
    search_set = SearchSet()
    cases_request = SearchCasesRequest()
    page_links = []

    try:
        search_set.read_user_args(request.args.to_dict())

        if search_set.search:
            cases_request.search_cases(search_set)
            if cases_request.cases:
                page_links = assemble_page_links(
                    pages_number=math.ceil(cases_request.cases_number / search_set.cases_per_page),
                    current_page=search_set.page,
                    url_args=search_set.args,
                )
            else:
                flash_message = 'Ничего не найдено.'

    except SearchArgsError as err:
        log.error(f'Location /. SearchArgsError. {err}')
        flash_message = 'Ошибка в аргументах поиска.'

    except RequestError as err:
        log.error(f'Location /. RequestError. {err}')
        flash_message = 'Ошибка запроса к базе.'

    except Exception as err:
        log.error(f'Location /. Exception. {err}')
        flash_message = 'Ошибка сервера.'

    return render_template(
        'index.html',
        flash_message=flash_message,
        search_args=search_set.args,
        cases=cases_request.cases,
        cases_number=cases_request.cases_number,
        end_cases_number=str(cases_request.cases_number)[-1],
        current_page=str(search_set.page),
        page_links=page_links,
    )


class SearchSet:
    """Поисковый набор."""

    args: dict
    search: dict
    page: int
    cases_per_page: int
    cases_skip: int

    def __init__(self) -> None:
        self.args = {}
        self.search = {}
        self.page = 1
        self.cases_per_page = app_config['cases_per_page']
        self.cases_skip = 0

    def read_user_args(self, args: dict) -> None:
        """Читает пользовательские аргументы запроса и собирает из них словарь с полями для поиска.

        :param args: Аргументы, переданные в запросе.
        :raise SearchArgsError: Что-то не так с аргументами поиска.
        """
        try:
            for arg_name, arg_value in args.items():

                if arg_name in ('defendant', 'plaintiff', 'category', 'office', 'number', 'article'):
                    self.args[arg_name] = escape(arg_value)
                    search_value: str = self._alphabet_nums_space_only(arg_value)
                    if search_value:
                        self.search[arg_name] = search_value

                elif arg_name in ('date_from', 'date_to'):
                    self.args[arg_name] = escape(arg_value)
                    if re.search(r'\d{4}-\d{1,2}-\d{1,2}', arg_value) is not None:
                        self.search[arg_name] = list(map(int, arg_value.split('-')))

                elif arg_name == 'page':
                    self.page = abs(int(arg_value))
                    if self.page == 0:
                        self.page = 1
                    self.cases_skip = (self.page - 1) * self.cases_per_page

        except (TypeError, ValueError, KeyError, IndexError) as err:
            log.error(f'Error while assemble a search set. {err}')
            self.search = {}
            raise SearchArgsError from err

    @staticmethod
    def _alphabet_nums_space_only(string: str) -> str:
        """Возвращает строку, в которой остались только цифры, буквы и пробелы.

        :param string: Исходная строка.
        :return: Строка без лишних символов.
        """
        filtered_chars = [a if a.isalnum() else ' ' for a in string]
        return ' '.join(''.join(filtered_chars).split())


class SearchCasesRequest:
    """Поиск судебных дел."""

    cases: list
    cases_number: int
    storage_url: str

    def __init__(self) -> None:
        self.cases = []
        self.cases_number = 0
        self.storage_url = app_config['storage_url']

    def search_cases(self, search_set: SearchSet) -> None:
        """Найти судебные дела.

        :param search_set: Объект SearchSet с параметрами поиска.
        :raise RequestError: Что-то пошло не так.
        """
        try:
            self.cases_number = self._request_cases_number(search_set.search)
            if self.cases_number > 0:
                self.cases = self._request_cases(
                    search_set.search,
                    skip=search_set.cases_skip,
                    limit=search_set.cases_per_page,
                )
        except json.JSONDecodeError as err:
            log.error(f'Search cases. JSONDecodeError "{err}".')
            raise RequestError from err
        except (ValueError, KeyError, TypeError) as err:
            log.error(f'Search cases. Error "{err}".')
            raise RequestError from err
        except requests.exceptions.RequestException as err:
            log.error(f'Search cases. RequestException "{err}".')
            raise RequestError from err

    def _request_cases_number(self, search: dict) -> int:
        response = requests.get(url=self.storage_url + '/case/quantity', json={'filter': search})
        if response.status_code != 200:
            raise RequestError('Request of cases number fail.')
        response_data: dict = response.json()
        return response_data['value']

    def _request_cases(self, search: dict, skip: int, limit: int) -> list:
        response = requests.get(url=self.storage_url + '/case', json={'filter': search, 'skip': skip, 'limit': limit})
        if response.status_code != 200:
            raise RequestError('Request of cases fail.')
        response_data: dict = response.json()
        return response_data['found_cases']


def assemble_page_links(pages_number: int, current_page: int, url_args: dict) -> list:
    """Возвращает список ссылок на страницы результатов поиска.

    :param pages_number: Количество найденных страниц.
    :param current_page: Номер текущей страницы.
    :param url_args: Аргументы строки запроса.
    :return: Список со ссылками на страницы.
    """
    if pages_number == 1:
        return []

    if pages_number <= 7:
        show_pages: set = set(range(1, pages_number + 1))
    else:
        show_pages: set = {1, pages_number}
        show_pages |= set(range(current_page - 1, current_page + 2))
        if current_page == 4:
            show_pages |= {2}
        if pages_number - current_page == 3:
            show_pages |= {pages_number - 1}
    show_pages = set(filter(lambda a: 1 <= a <= pages_number, show_pages))

    page_links = []
    for page in sorted(show_pages):
        if page_links and (page - page_links[-1]['page'] or page) > 1:
            page_links.append({'page': None})
        if page == current_page:
            page_links.append({'page': page, 'current': True})
        else:
            page_links.append({'page': page, 'url': url_for('get_root', _external=True, **url_args, page=page)})

    return page_links


def main() -> None:
    app.run(
        debug=(app_config['log_level'] == 'DEBUG'),
        host=app_config['bind_host'],
        port=app_config['bind_port']
    )


if __name__ == '__main__':
    main()
