import os

__doc__: str = """Конфигурация приложения.
Все опции, перечисленные здесь, можно переопределить переменными окружения.
Например опция log_level соответствует переменной COURTS_LOG_LEVEL.
"""

default: tuple = (
    # Слушать этот адрес.
    ('bind_host', '0.0.0.0', None),
    # Слушать этот порт.
    ('bind_port', '80', int),
    # Уровень логирования.
    ('log_level', 'WARNING', lambda a: a.upper()),
    # Адрес хранилища судебных дел.
    ('storage_url', 'http://localhost:80', None),
    # Количество судебных дел в одной странице.
    ('cases_per_page', 50, int)
)
app_config: dict = {}
for key, value, func in default:
    app_config[key] = os.getenv('COURTS_' + key.upper(), value)
    if func is not None:
        app_config[key] = func(app_config[key])

if __name__ == '__main__':
    from pprint import pprint
    pprint(app_config)
