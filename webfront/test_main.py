import pytest
import flask.testing
import main
from main import app
from main import SearchSet, SearchArgsError, assemble_page_links


def mock_url_for(func_name: str, **kwargs) -> str:
    """Подмена функции генерации url. Нужна, так как оригинальная функция ругается на отсутствие контекста."""
    return f'http/{func_name}/page={kwargs["page"]}'


main.url_for = mock_url_for


class TestSearchSet:
    """Класс компиляции поискового набора."""

    @pytest.fixture
    def search_set(self) -> SearchSet:
        return SearchSet()

    def test_alphabet_nums_space_only(self, search_set) -> None:
        """Строки запроса с лишними символами и без."""
        result = search_set._alphabet_nums_space_only('')
        assert result == ''

        result = search_set._alphabet_nums_space_only('foo')
        assert result == 'foo'

        result = search_set._alphabet_nums_space_only('foo bar')
        assert result == 'foo bar'

        result = search_set._alphabet_nums_space_only(' foo bar  %$# world....')
        assert result == 'foo bar world'

        assert search_set.search == {}

    def test_read_user_args_valid(self, search_set) -> None:
        """Правильные аргументы запроса."""
        search_set.read_user_args({})
        assert search_set.args == {}
        assert search_set.search == {}

        search_set.read_user_args({
            'unknown_field': '...',
            'defendant': 'Иванов И.И."; DROP TABLE users;',
            'plaintiff': 'ГУИ МУИ',
            'office': 'Земская street, 42',
            'category': 'Судебные решения. и пр.',
            'number': '1-234/gg',
            'article': 'ст.282, ч.1',
            'date_from': '2021-12-3',
            'date_to': '2021-12-4',
        })
        assert search_set.args == {
            'defendant': 'Иванов И.И.&#34;; DROP TABLE users;',
            'plaintiff': 'ГУИ МУИ',
            'office': 'Земская street, 42',
            'category': 'Судебные решения. и пр.',
            'number': '1-234/gg',
            'article': 'ст.282, ч.1',
            'date_from': '2021-12-3',
            'date_to': '2021-12-4',
        }
        assert search_set.search == {
            'defendant': 'Иванов И И DROP TABLE users',
            'plaintiff': 'ГУИ МУИ',
            'office': 'Земская street 42',
            'category': 'Судебные решения и пр',
            'number': '1 234 gg',
            'article': 'ст 282 ч 1',
            'date_from': [2021, 12, 3],
            'date_to': [2021, 12, 4],
        }

    def test_read_user_args_invalid(self, search_set) -> None:
        """Неправильные аргументы запроса."""

        search_set.read_user_args({'key1': '', 'key2': 42})
        assert search_set.args == {}
        assert search_set.search == {}

        with pytest.raises(SearchArgsError):
            search_set.read_user_args({'page': 'foo'})
        assert search_set.search == {}


class TestAssemblePageLinks:
    """Функция формирования ссылки на страницу."""

    def test_one_page(self) -> None:
        """Одна страница."""
        assert assemble_page_links(pages_number=1, current_page=1, url_args={}) == []

    def test_less_equal_than_seven_pages(self) -> None:
        """До семи страниц."""
        assert assemble_page_links(pages_number=2, current_page=1, url_args={}) == [
            {'page': 1, 'current': True},
            {'page': 2, 'url': 'http/get_root/page=2'},
        ]
        assert assemble_page_links(pages_number=4, current_page=3, url_args={}) == [
            {'page': 1, 'url': 'http/get_root/page=1'},
            {'page': 2, 'url': 'http/get_root/page=2'},
            {'page': 3, 'current': True},
            {'page': 4, 'url': 'http/get_root/page=4'},
        ]
        assert assemble_page_links(pages_number=7, current_page=6, url_args={}) == [
            {'page': 1, 'url': 'http/get_root/page=1'},
            {'page': 2, 'url': 'http/get_root/page=2'},
            {'page': 3, 'url': 'http/get_root/page=3'},
            {'page': 4, 'url': 'http/get_root/page=4'},
            {'page': 5, 'url': 'http/get_root/page=5'},
            {'page': 6, 'current': True},
            {'page': 7, 'url': 'http/get_root/page=7'},
        ]

    def test_more_than_seven_pages(self) -> None:
        """Более семи страниц."""
        assert assemble_page_links(pages_number=16, current_page=4, url_args={}) == [
            {'page': 1, 'url': 'http/get_root/page=1'},
            {'page': 2, 'url': 'http/get_root/page=2'},
            {'page': 3, 'url': 'http/get_root/page=3'},
            {'page': 4, 'current': True},
            {'page': 5, 'url': 'http/get_root/page=5'},
            {'page': None},
            {'page': 16, 'url': 'http/get_root/page=16'},
        ]
        assert assemble_page_links(pages_number=16, current_page=10, url_args={}) == [
            {'page': 1, 'url': 'http/get_root/page=1'},
            {'page': None},
            {'page': 9, 'url': 'http/get_root/page=9'},
            {'page': 10, 'current': True},
            {'page': 11, 'url': 'http/get_root/page=11'},
            {'page': None},
            {'page': 16, 'url': 'http/get_root/page=16'},
        ]
        assert assemble_page_links(pages_number=16, current_page=16, url_args={}) == [
            {'page': 1, 'url': 'http/get_root/page=1'},
            {'page': None},
            {'page': 15, 'url': 'http/get_root/page=15'},
            {'page': 16, 'current': True},
        ]


class TestGetRoot:

    @pytest.fixture
    def client(self) -> flask.testing.FlaskClient:
        with app.test_client() as client:
            yield client

    def test_valid(self, client) -> None:
        """Запрос страницы поиска без параметров."""
        answer = client.get('/')
        assert answer.status_code == 200

    def test_invalid_args(self, client) -> None:
        """Запрос страницы поиска с некорректными параметрами."""
        answer = client.get('/', json={'foo': 'bar'})
        assert answer.status_code == 200

        answer = client.get('/', json=['foo', 'bar'])
        assert answer.status_code == 200
